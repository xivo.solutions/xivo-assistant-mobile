var PROD = process.argv.indexOf("--production") > -1;
var config = require('./package.json');
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var WebpackNotifierPlugin = require('webpack-notifier');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var Clean = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var localIdentName = !PROD ? '?localIdentName=[name]_[local]-[hash:base64:3]' : '';

var extractCSS = new ExtractTextPlugin('style.css', {
    disable: !PROD,
    allChunks: PROD
});
var plugins = {
    production: [
        new Clean(['dist']),
        extractCSS,
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            sourceMap: false
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.join(__dirname, 'src', 'templates', 'index.jade'),
            minify: false,
            chunksSortMode: 'none',
            inject: false
        }),
        new webpack.DefinePlugin({
            __PRODUCTION__: JSON.stringify(PROD),
            __DEV__: JSON.stringify(!PROD),
        })
    ],
    developpement: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        extractCSS,
        new WebpackNotifierPlugin({
            title: config.name,
            excludeWarnings: true
        }),
        new webpack.DefinePlugin({
            __PRODUCTION__: JSON.stringify(PROD),
            __DEV__: JSON.stringify(!PROD),
        })
    ]
};

module.exports = {
    devtool: (!PROD ? 'eval-source-map' : 'eval'),
    entry: [path.join(__dirname, 'src', 'app.js')],
    target: 'web',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve: {
        alias: {
            'src': path.join(__dirname, 'src'),
            'scss': path.join(__dirname, 'src', 'scss'),
            'assets': path.join(__dirname, 'src', 'assets'),
            'containers': path.join(__dirname, 'src', 'containers'),
            'components': path.join(__dirname, 'src', 'components'),
            'fonts': path.join(__dirname, 'src', 'assets', 'fonts')
        }
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            },
            {
                test: /\.json$/,
                loaders: ['json']
            },
            {
                test: /\.(ico|jpe?g|png|gif)$/,
                loaders: [
                  'file?name=[path][name].[ext]&context=./src'
                ]
            },
            {
                test: /\.(woff|woff2|ttf|otf|eot|svg|mp4|mov)$/,
                loaders: [
                  "file?name=[path][name].[ext]&context=./src",
                ],
            },
            {
                test: /\.jade$/,
                loader: 'jade'
            },
            {
                test: /\.css$/,
                loader: PROD ? extractCSS.extract('css') : 'style!css'
            },
            {
                test: /\.scss$/i,
                 loader: PROD ? extractCSS.extract('css!sass!postcss') : 'style!css'+localIdentName+'!sass!postcss'
            }
        ]
    },
    plugins: PROD ? plugins.production : plugins.developpement
};
