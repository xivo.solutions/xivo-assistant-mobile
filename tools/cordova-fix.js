var fs = require('fs');
var path = require('path');
var public_folder = 'xivomobility-cordova/www/';
var public_path = path.join(__dirname, '../' + public_folder);
var index_html_path = path.join(public_path, 'index.html');

fs.readFile(index_html_path, 'utf8', function(err, file) {
	if(err) {
		console.log('Writing error');
		return false;
	} else {
		performFix(file);
	}
});




function performFix(index_html) {
	var tag = '<!-- CORDOVA -->';
	var cordova_script = '<script src="cordova.js"></script>';
	var toString = index_html.toString();

	fs.writeFile(index_html_path, toString.replace(tag, cordova_script), 'utf8', function(err, success){
		if(err) {
			console.log('Writing error !');
			return false;
		} else {
			console.log('Cordova script added !');
		}
	});
}
