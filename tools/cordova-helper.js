
var config = require('../package.json');
var process = require('process');
var child_process = require('child_process');

var plugin_to_install = config.cordovaPlugins;
var project_name = config.name;
var args = process.argv.slice(2);



if (args[0] === 'sync') {
	//RSYNC files

	 var cordova_sync = child_process.execSync('rsync -r dist/* '+ config.cordova.folderName +'/www');
	console.log(cordova_sync.toString());
}


if (args[0] === 'init') {
	var cordova_init = child_process.execSync('cordova create '+ config.cordova.folderName +' '+ config.cordova.bundleID +' \"'+ config.cordova.applicationName +'\"');
	console.log(cordova_init.toString());

	var cordova_platforms = child_process.execSync('cd '+ config.cordova.folderName +' && cordova platform add '+ config.cordova.platforms.join(" "));
	console.log(cordova_platforms.toString());
}

if (args[0] === 'install') {
	//Install cordova & plugins
	if (typeof config.cordova.plugins === 'undefined') { return false; }

	for(var i in config.cordova.plugins) {
		var plugin_name = i;
		var plugin_version = config.cordova.plugins[i];
		
		var plugin_out = child_process.execSync('cd '+ config.cordova.folderName +' && cordova plugin add ' + plugin_name + '@' + plugin_version);
		console.log(plugin_out.toString());
		console.log('CORDOVA Installer: -> ' + plugin_name + '@' + plugin_version);
	}

	console.log('CORDOVA Installer: finished');

}

if (args[0] === 'fix') {
	//Some fixes
}

