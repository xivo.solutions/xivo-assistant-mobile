var http = require('http');
var express = require('express');
var app = express();

const hostname = process.env.HOSTNAME || "localhost";
const port     = process.env.PORT || 3000;

//app.use(require('morgan')('short'));

(function() {

  var webpack = require('webpack');
  var webpackConfig = require('./webpack.client.config');
  webpackConfig.entry.unshift('webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000');
  var compiler = webpack(webpackConfig);

  app.use(require("webpack-dev-middleware")(compiler, {
    noInfo: true, publicPath: webpackConfig.output.publicPath,
    stats: {
        colors: true
    }
  }));

  app.use(require("webpack-hot-middleware")(compiler, {
    log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
  }));
})();


app.set('views', 'src/templates');
app.set('view engine', 'jade');
app.use(express.static('dist/assets'));
app.use(express.static('dist'));
app.use(express.static('src'));

app.get("/", function(req, res) {
  res.render('index', {data: {}});
});

if (require.main === module) {
  app.listen(port, function() {
    console.info("==> 🔨  Developpement server running");
    console.info("==> 🌎  Go to http://%s:%s", hostname, port);
  });
}
