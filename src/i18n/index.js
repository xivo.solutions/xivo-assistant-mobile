

class i18n {

	loadLocalization(lang = 'fr') {
		this.lang = lang;
		this.locale = require(`src/i18n/${lang}_locale`).default;
	}

	translate(key, vars = {}) {
		let translation = key;

		if (typeof this.locale[key] !== 'undefined') {
			if (this.locale[key].length > 0) {
				translation = this.locale[key].replace(/\${([a-z0-9_]+)\}/, function (match, match1) {
					if (typeof vars[match1] !== 'undefined') {
						return vars[match1];
					}
		        });
			}
			
		}
		
		return translation;
	}

};



export default new i18n();