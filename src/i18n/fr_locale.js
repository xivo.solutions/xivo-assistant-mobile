export default {
	LOGIN_TEXT: 'Simplicité, évolutivité, performance : bienvenue sur votre espace XiVO. La solution Open Source de téléphonie au service de votre entreprise.',
	LOGGED_TEXT: 'Bienvenue ${name} !',
	SIGN_IN: 'CONNEXION',
	LOGIN: 'Identifiant',
	PASSWORD: 'Mot de passe',
	SIGN_IN_ME: 'Me connecter',
	UNKNOWN: 'Inconnu',
	SERVER: 'Saisissez l\'URL de votre serveur',
	SERVER_TIMEOUT: 'Connexion au serveur  <br /> impossible',
	SERVER_LOGIN_ERROR: 'Utilisateur non reconnu <br /> ou <br /> Mauvais mot de passe',
	SERVER_CONNECTING: 'Connexion en cours',

	VOICEMAIL: 'Messagerie',
	VOICEMAIL_MULTIPLE: 'Vous avez ${number} nouveaux messages',
	VOICEMAIL_SIMPLE: 'Vous avez ${number} nouveau message',

	NUMBER_CALL_TXT: 'Numéro :',
	UNKNOWN_CALL_IMPOSSIBLE: 'Impossible d\'appeler un numéro inconnu',

	CALL_HISTORY_EMPTY: 'Pas d\'historique d\'appel',
	FAVORITES_EMPTY: 'Aucun favoris',

	SEARCH_MIN_LENGTH: 'Vous devez entrer plus de 2 caractères pour faire une recherche.',

	CALL: 'appeler',
	CANCEL: 'annuler',
	OK: 'ok',
	VALIDATE: 'valider',

	MENU_SECTION_PREFS: 'Préférences',
	MENU_SECTION_SETTINGS: 'Paramètres',

	MENU_USE_PHONE_CONTACTS: 'Utiliser les contacts du téléphone',
	MENU_CALL_FORWARDING: 'Numéro de renvoi :',
	MENU_XIVO_ADDRESS: 'Adresse du serveur XiVO',
	MENU_WIFI_HOTSPOT: 'Point d\'accès WIFI autorisé',
	MENU_CALLBACK_NUMBER: 'N° de mobile utilisé pour le call back',
	MENU_LOGOUT: 'Déconnexion',

	LOADING: 'Chargement...',
	CALL_HISTORY: 'Historique d\'appels',
	FAVORITES: 'Favoris',
	SEARCH: 'Recherche',

	MOMENT_HISTORY_FORMAT: 'D MMMM YYYY',
	MOMENT_HISTORY_NOW: 'Aujourd\'hui',

	INCALL: 'Appel en cours',
	INTRANSIT: '"XiVO vous rappelle" <br />  Décrochez pour être mis en relation avec votre contact',
	OUTGOING: 'Appel sortant',
	RINGING: 'Sonnerie en cours',

	MULTIPLE_NUMBERS_SELECTION: '${username}<br /> a plusieurs numéros. <br /> Lequel voulez-vous joindre ?',

	VALIDATE: 'VALIDER',

	PHONE_USE_TYPE: 'TYPE D\'UTILISATION',
	PHONE_USE_TYPE_MODAL_TXT: 'Comment souhaitez-vous <br /> gérer vos appels ?',
	PHONE_USE_TYPE_MODAL_ERROR_TXT: 'Vous devez saisir un numéro de téléphone pour pouvoir faire un renvoi d\'appel',

	PHONE_FORWARD: 'Renvoi d\'appel',
	PHONE_FORWARD_MODAL_TXT: 'Vers quel numéro souhaitez-vous renvoyer les appels ?',
	
	SERVER_KIND: 'Type de serveur :',
	SERVER_URL: 'URL du serveur :',

	OPTIONS_TITLE: 'OPTIONS AVANCÉES'
};
