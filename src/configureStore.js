import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import {routerReducer, routerMiddleware } from 'react-router-redux';
import promiseMiddleware from 'redux-promise';
import * as appReducers from 'src/reducers';

import DevTools from 'containers/tools/DevTools';

export default function configureStore(initialState, history) {

    const loggerMiddleware = createLogger();

    const reducers = combineReducers({
        ...appReducers,
        routing: routerReducer
    });

    const middleware = applyMiddleware(routerMiddleware(history), loggerMiddleware, thunkMiddleware, promiseMiddleware);

    const enhancer = compose(
        middleware,
        DevTools.instrument()
    );

    const store = createStore(reducers, initialState, enhancer);
    // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
    if (module.hot) {
        module.hot.accept('./reducers', () =>
            store.replaceReducer(require('./reducers')/*.default if you use Babel 6+ */)
        );
    }

    return store;
}
