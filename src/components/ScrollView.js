import React, {Component} from 'react';

import style from 'scss/scrollview.scss';

export default class ScrollView extends Component {

    render() {
        let {children, ...props} = this.props;

        return (
            <div className={style.container} {...props}>
                {children}
            </div>
        );
    }
}
