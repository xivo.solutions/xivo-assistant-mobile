import React, {Component} from 'react';

export default class Cordova extends Component {

	constructor() {
		super();
		this.keyboardEvents = false;
	}

	componentDidMount() {
		let {onKeyboard} = this.props;
		if (typeof cordova !== 'undefined' && onKeyboard && !this.keyboardEvents) {
			window.removeEventListener('native.keyboardshow');
			window.removeEventListener('native.keyboardhide');

			window.addEventListener('native.keyboardshow', (e) => {
				onKeyboard.apply(this, ['show', e.keyboardHeight]);
			});

			window.addEventListener('native.keyboardhide', () => {
				onKeyboard.apply(this, ['hide', 0]);
			});

			this.keyboardEvents = true;
		}

	}

    render() {
        let {children, onKeyboard, ...props} = this.props;

        return (
            <div {...props}>
                {children}
            </div>
        );
    }
}
