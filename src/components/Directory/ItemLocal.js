import React, {Component} from 'react';
import style from 'scss/favorites.scss'
import classNames from 'classnames';

export default class DirectoryItem extends Component {

    onClickItem() {
        this.props.onClickItem();
    }

    render() {
        let {item, onClickItem} = this.props;
        console.log('render', item);
        return (
            <div className={style.item} onClick={this.onClickItem.bind(this)}>
                <span className={style.status}></span>
                <span className={style.firstname}>{item.name.formatted}</span>
            </div>
        );
    }
}