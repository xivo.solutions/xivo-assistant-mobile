import React, {Component} from 'react';
import style from 'scss/favorites.scss';
import {PHONE_STATUS_COLOR} from 'src/constants';
import classNames from 'classnames';
import Cti from 'src/helpers/Cti';

export default class DirectoryItem extends Component {

    onClickItem() {
        this.props.onClickItem();
    }

    render() {
        let {item, onClickItem} = this.props;
        let {entry, favorite, contact_id, source, status} = item;
        let is_favorite_class = classNames(style.fav, {
            [`${style.fav_active}`] : favorite
        });

        let status_color_style = {};

        /*if (typeof PHONE_STATUS_COLOR[status] !== 'undefined') {
            status_color_style.backgroundColor = PHONE_STATUS_COLOR[status];
        }*/

        if (typeof PHONE_STATUS_COLOR[status] !== 'undefined') {
            status_color_style.backgroundColor = PHONE_STATUS_COLOR[status];
        }
        var name = '';
        if (typeof entry !== 'undefined' && entry.length > 0) {
            name = entry[0];
        }

        console.log(item);

        return (
            <div className={style.item}>
                <span className={style.status} style={status_color_style}></span>
                <span onClick={this.props.onFavoriteButton.bind(this, contact_id, source, favorite)} className={is_favorite_class}>
                    <i className={'fa fa-star'} />
                </span>
                <span onClick={this.onClickItem.bind(this)} className={style.firstname}>{entry[0]}</span>
            </div>
        );
    }
}