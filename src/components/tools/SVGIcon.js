import React, {Component} from 'react';

export default class SVGIcon extends Component {
    render() {
        let {filename, ...props} = this.props;

        return (
            <img {...props} src={require(`assets/svg/${filename}.svg`)} />
        );
    }
}
