import React, {Component} from 'react';
import classNames from 'classnames';
import style from 'scss/button.scss';

export default class Button extends Component {

	constructor() {
		super();
        this.state = {
            active: false
        };
	}

	componentDidMount() {

	}

    onTouchStart() {
        this.setState({active: true});
    }

    onTouchEnd() {
        this.setState({active: false});
    }

    render() {
        let {children, type, size, ...props} = this.props;
        let type_class = [];
        if (type === 'cancel') {
            type_class.push(style.btn_cancel);
        } else {
            type_class.push(style.btn_default);
        }

        if (size === 'small') {
            type_class.push(style.small);
        }




        let btn_classes = classNames(style.btn, type_class, {
            [`${style.active}`]: this.state.active
        });

        return (
            <div className={btn_classes} {...props} onTouchStart={this.onTouchStart.bind(this)} onTouchEnd={this.onTouchEnd.bind(this)}>
                {children}
            </div>
        );
    }
}

