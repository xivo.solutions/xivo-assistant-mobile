import React, {Component} from 'react';
import classNames from 'classnames';
import style from 'scss/button.scss';

export default class TouchableItem extends Component {

	constructor() {
		super();
        this.state = {
            active: false
        }
	}

	componentDidMount() {

	}

    onTouchStart() {
        this.setState({active: true});
    }

    onTouchEnd() {
        this.setState({active: false});
    }

    render() {
        let {children, ...props} = this.props;
        return (
            <div {...props} onTouchStart={this.onTouchStart.bind(this)} onTouchEnd={this.onTouchEnd.bind(this)}>
                {children}
            </div>
        );
    }
}
