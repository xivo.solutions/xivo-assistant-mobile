import React, {Component} from 'react';
import classNames from 'classnames';
import style from 'scss/touchable_link.scss';
import { Link } from 'react-router';

export default class TouchableLink extends Component {

	constructor() {
		super();
        this.state = {
            active: false
        }
	}

	componentDidMount() {

	}

    onTouchStart() {
        this.setState({active: true});
    }

    onTouchEnd() {
        this.setState({active: false});
    }

    render() {
        let {children, ...props} = this.props;
        let link_classes = classNames(style.round_btn, {
            [`${style.active}`]: this.state.active
        });
        return (
            <Link {...props} className={link_classes} activeClassName={style.round_btn_active} onTouchStart={this.onTouchStart.bind(this)} onTouchEnd={this.onTouchEnd.bind(this)}>
                {children}
            </Link>
        );
    }
}
