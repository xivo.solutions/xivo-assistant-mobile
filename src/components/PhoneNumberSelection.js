import React, {Component} from 'react';
import classNames from 'classnames';
import style from 'scss/modal.scss';
import i18n from 'src/i18n';
import Button from 'components/Button';

export default class Modal extends Component {

    constructor() {
        super();
    }

    componentDidMount() {

    }

    render() {
        let {numbers, username, onClose, onDial} = this.props;

        let numbers_items = numbers.map((number, index) => (<Button key={index} onClick={onDial.bind(this, number, name)}>{number}</Button>));

        return (
            <div>
                <p dangerouslySetInnerHTML={{__html: i18n.translate('MULTIPLE_NUMBERS_SELECTION', {username: username})}} />
                {numbers_items}
                <Button type="cancel" onClick={onClose.bind(this)}>{i18n.translate('CANCEL')}</Button>
            </div>
        );
    }
}
