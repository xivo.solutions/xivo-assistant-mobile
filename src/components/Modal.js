import React, {Component} from 'react';
import classNames from 'classnames';
import style from 'scss/modal.scss';

export default class Modal extends Component {

	constructor() {
		super();
	}

	componentDidMount() {

	}

    render() {
        let {children, noStyle, hide, ...props} = this.props;
        let classes = classNames(style.container, {
        	[`${style.hide}`]: hide,
            [`${style.modal}`]: !noStyle
        });

    

        return (
            <div className={classes} {...props}>
                {children}
            </div>
        );
    }
}
