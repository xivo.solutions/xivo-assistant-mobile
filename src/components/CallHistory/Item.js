import React, {Component} from 'react';
import moment from 'moment';
import classNames from 'classnames';
import i18n from 'src/i18n';
import SVGIcon from 'components/tools/SVGIcon';
import style from 'scss/call_history.scss';

export default class CallHistoryItem extends Component {
    constructor() {
        super();
        this.state = {
            active: false
        };
    }

    formatDuration(duration) {
        let separator = ':';
        if (duration) {
            let splitted_duration = duration.split(separator);
            if (parseInt(splitted_duration[0], 10) === 0) {
                splitted_duration.shift();
                duration = splitted_duration.join(separator);
            }
        }
        return duration;
    }

    formatStartTime(start) {
        start = moment(start).format('HH:mm');
        return start;
    }

    onTouchStart() {
        this.setState({active: true});
    }

    onTouchEnd() {
        this.setState({active: false});
    }

    render() {
        let {onItemClick, item} = this.props
        let {status, dstNum, start, duration, dstFirstName, dstLastName} = item;
        let call_icon = '';
        
        switch (status) {
            case 'emitted':
                call_icon = "outgoing_call";
                break;
            case 'missed':
                call_icon = "call_missed";
                break;
            default:
                call_icon = "incoming_call";
        }

        let item_classes = classNames(style.item, {
            [`${style.active}`]: this.state.active
        })
        
        let contact_name = i18n.translate('UNKNOWN');
        if (dstLastName !== null) {
            contact_name = dstLastName;
            if (dstFirstName !== null) {
                contact_name = contact_name + ' ' + dstFirstName;
            }
        } else {
            if (dstNum !== null) {
                contact_name = dstNum;
            }
        }


        return (
            <div className={item_classes} onClick={onItemClick} onTouchStart={this.onTouchStart.bind(this)} onTouchEnd={this.onTouchEnd.bind(this)}>
                <p className={style.item_dstNum}>{contact_name}</p>

                <div className={style.call_info}>
                   
                    <p className={style.item_status}>
                        <SVGIcon filename={call_icon} width={30}/>
                    </p>
                    <p className={style.item_duration}>{this.formatStartTime(start)}</p>
                </div>
                
            </div>
        );
    }
}