import React, {Component} from 'react';
import classNames from 'classnames';
import SVGIcon from 'components/tools/SVGIcon';

export default class InProgressCall extends Component {

	constructor() {
		super();
        this.max_image = 8;
        this.state = {
            animate: false,
            current_image: 0
        };
	}


    componentWillUnmount() {
        //clearTimeout(this.animation);
    }

    componentDidMount() {
        let _this = this;
        /*this.animation = setTimeout(function(){
           
            if (_this.state.current_image < _this.max_image) {
                _this.setState({
                    current_image: _this.state.current_image + 1
                });
            } else {
                _this.setState({
                    current_image: 0
                });
            }

            clearTimeout(_this.animation);
        }, 200);*/
    }

	componentDidUpdate() {
        let _this = this;

	}

    render() {
        let {children, ...props} = this.props;
        return (
            <SVGIcon filename={`callback_anim/callback_anim_${this.state.current_image}`} width={110}/>
        );
    }
}
