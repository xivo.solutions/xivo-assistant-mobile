import React, {Component} from 'react';
import classNames from 'classnames';
import SVGIcon from 'components/tools/SVGIcon';
import {STATUS} from 'src/constants';
import InProgressCall from 'components/InCall/InProgressCall';

export default class CallStateCloud extends Component {

	constructor() {
		super();
        this.max_image = 8;
        this.state = {
            animate: false,
            current_image: 0
        };
	}

	componentDidUpdate() {
        let _this = this;
	}

    render() {
        let {children, lineState, ...props} = this.props;

        let filename = (<InProgressCall />);

        if (lineState === STATUS.RINGING) {
            filename = (<SVGIcon filename={`outgoingcall_ringing`} width={110}/>);
        } else if (lineState === STATUS.UP) {
            filename = (<SVGIcon filename={`incoming_incall`} width={110}/>);
        }


        return (<div>
            {filename}
            </div>
        );
    }
}
