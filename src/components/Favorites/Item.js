import React, {Component} from 'react';
import style from 'scss/favorites.scss';
import {PHONE_STATUS_COLOR} from 'src/constants';
import classNames from 'classnames';

export default class FavoriteItem extends Component {

    render() {
        let {item} = this.props;
        let {entry, favorite, contact_id, source, status} = item;
        let is_favorite_class = classNames(style.fav, {
            [`${style.fav_active}`] : favorite
        });

        let status_color_style = {};

        if (typeof PHONE_STATUS_COLOR[status] !== 'undefined') {
            status_color_style.backgroundColor = PHONE_STATUS_COLOR[status];
        }

        return (
            <div className={style.item} >
                <span className={style.status} style={status_color_style}></span>
                <span onClick={this.props.onFavoriteButton.bind(this, contact_id, source)} className={is_favorite_class}>
                    <i className={'fa fa-star'} />
                </span>
                <span onClick={this.props.onItem.bind(this, this.props.item)} className={style.firstname}>{entry[0]}</span>
            </div>
        );
    }
}