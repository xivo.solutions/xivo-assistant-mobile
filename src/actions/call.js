import {CALL_CALLING, CALL_HISTORY, DIRECTORY_RESULT, CALL_DIAL, CALL_STATUS, CALL_UPDATE, CALL_END_DIAL} from 'src/actionTypes';
import {PHONE_EVENT, STATUS, getConstant} from 'src/constants';
import Cti from 'src/helpers/Cti';
import LocalDatas from 'src/LocalDatas';


var dispatch_tmp = (function() {});

const onPhoneStatusUpdate = (status) => {
    dispatch_tmp({
        type: CALL_STATUS,
        status
    });
};


const onPhoneEvent = (e) => {
    /*dispatch({
        type: CALL_UPDATE,
        event: {
            from: e.DN,
            top: e.otherDN,
            userData: e.userData,
            type: e.eventType
        }
    });

    if (e.eventType === PHONE_EVENT.RELEASED) {
        setTimeout(function() {
            dispatch({
                type: CALL_UPDATE,
                event: {}
            });
        }, 2000);
    }*/
};

const onLineStateEvent = (e) => {
    dispatch_tmp({
        type: CALL_UPDATE,
        event: {
            to: e.lineNumber,
            lineState: e.callData.lineState,
        }
    });

    if (e.callData.lineState === STATUS.HUNGUP) {
        setTimeout(function() {
            dispatch_tmp({
                type: CALL_UPDATE,
                event: {}
            });
        }, 500);
    }
};



const onQueueStatistics = () => {
     //console.log('QUEUESTATISTICS', e);
};

const onSheet = (e) => {
};

const onLinkStatusUpdate = (e) => {

};

export const initEvents = () => {
    window.Cti = Cti;
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }
   

    Cti.subscribeToQueueStats();
    Cti.getList("queue");
    

    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, onPhoneStatusUpdate);
    Cti.setHandler(Cti.MessageType.PHONEEVENT, onPhoneEvent);
    Cti.setHandler('LineStateEvent', onLineStateEvent);
    //Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, onVoiceMailStatusUpdate);
    Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, onQueueStatistics);
    Cti.setHandler(Cti.MessageType.SHEET, onSheet);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, onLinkStatusUpdate);

    return (dispatch) => {dispatch_tmp = dispatch;}
};


function onCallHistory (history_list){
    dispatch_tmp({
        type: CALL_HISTORY,
        data: history_list
    });
};
export const getHistory = function () {
   if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    


    Cti.getUserCallHistory(100);
    Cti.setHandler(Cti.MessageType.CALLHISTORY, onCallHistory);
    return (dispatch) => { dispatch_tmp = dispatch;};
};


function onDirectoryResult (directory) {
    dispatch_tmp({
        type: DIRECTORY_RESULT,
        data: directory
    });
};
export const getFavorites = () => {
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.getFavorites();
    Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, onDirectoryResult);
    return (dispatch) => { dispatch_tmp = dispatch;};
};

export const dial = (number, name, from_mobile = false) => {
   if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    if (from_mobile) {
        Cti.dialFromMobile(number);
    } else {
        Cti.dial(number);
    }
    
    return {
        type: CALL_DIAL,
        number, 
        name
    };
};


export const setForwardNumber = (destination) => {
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    let forward_config = LocalDatas.get('fwd') || getConstant('FWD');
    let new_config = {
        ...forward_config, 
        destination: destination
    };
    LocalDatas.set('fwd', new_config)
    Cti.uncFwd(destination, forward_config.enabled);

    return {
        type: ''
    };

};

export const isCalling = (calling) => {
    return {
        type: CALL_CALLING,
        calling
    }
};


export const enableForward = (enable) => {
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }
    let forward_config = LocalDatas.get('fwd') || getConstant('FWD');
    Cti.uncFwd(forward_config.destination, enable);
    return {
        type: ''
    };
};



export const hangup = () => {
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.hangup();
    return (dispatch) => {
        dispatch({type: CALL_END_DIAL});
        dispatch({
            type: CALL_UPDATE,
            event: {}
        });
    };
};

