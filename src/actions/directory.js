import {DIRECTORY_XIVO_DATA, DIRECTORY_LOCAL_DATA, DIRECTORY_RESET} from 'src/actionTypes';
import Cti from 'src/helpers/Cti';
import validator from 'validator';
import libphonenumber from 'google-libphonenumber';
var dispatch_tmp = (function() {});


window.libphonenumber = libphonenumber;
window.validator = validator;

export const resetDirectory = () => {
    return {
        type: DIRECTORY_RESET
    };
};
var search_term = '';
export const search = (term, local = false) => {
    search_term = term;
    return (dispatch) => {
        dispatch_tmp = dispatch;
        resetDirectory();
        searchInXivoDirectory(term, dispatch);
        if (local) {
            searchInLocalDirectory(term, dispatch);
        } else {

            let data = [];
            
            setTimeout(function() {
                dispatch({
                    type: DIRECTORY_LOCAL_DATA,
                    data: data,
                    search: search_term
                });
            }, 200);
        }
    };
};

const phoneNumberHandler = (phone) => {
    let data = [];
    /*let phone_util = libphonenumber.PhoneNumberUtil.getInstance();
    
    let phone_number = phone_util.format(phone_util.parse(phone, 'FR'), 0);
    console.log(phone_util.parse(phone, 'FR'));
    if (validator.isMobilePhone(phone_number, 'fr-FR')) {
        let formatted_phone_number = phone_util.format(phone_number, 1);
        data.push({
            phoneNumbers: [{
                value: formatted_phone_number
            }],
            name: {
                formatted: formatted_phone_number
            }
        });
    }*/


    data.push({
        phoneNumbers: [{
            value: phone
        }],
        name: {
            formatted: phone
        }
    });

    return data;
};

const onDirectoryResult = (directory) => {

    let contact = directory.entries;
    console.log('search_term', search_term);
    dispatch_tmp({
        type: DIRECTORY_XIVO_DATA,
        data: contact,
        search: search_term
    });
};

const searchInXivoDirectory = (term, dispatch) => {
 if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }
    Cti.directoryLookUp(term);
    search_term = term;
    Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, onDirectoryResult);
};

const searchInLocalDirectory = (term, dispatch) => {
    function onSuccess(contacts) {
        console.log('contacts', contacts);
        contacts = contacts.filter((contact) => {
            return contact.phoneNumbers !== null
        });


         dispatch({
            type: DIRECTORY_LOCAL_DATA,
            data: contacts,
            search: search_term
        });
    };

    function onError(contactError) {
       console.log('error', contactError);
    };



    if (typeof cordova !== 'undefined') {
        var options = new ContactFindOptions();
        options.filter = term;
        options.multiple = true;

        var filters = [
            navigator.contacts.fieldType.displayName,
            navigator.contacts.fieldType.formatted,
            navigator.contacts.fieldType.givenName,
            navigator.contacts.fieldType.middleName,
            navigator.contacts.fieldType.nickname,
            navigator.contacts.fieldType.name,
            navigator.contacts.fieldType.title,
            navigator.contacts.fieldType.phoneNumbers,
            navigator.contacts.fieldType.phoneNumbers
        ];

        navigator.contacts.find(filters, onSuccess, onError, options);
    } else {
        setTimeout(function() {
            dispatch({
                type: DIRECTORY_LOCAL_DATA,
                data: [],
                search: search_term
            });
        }, 200);
    }
};