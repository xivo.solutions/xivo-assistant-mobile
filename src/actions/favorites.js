import {DIRECTORY_STATUS_UPDATE, DIRECTORY_DATA_UPDATED, FAVORITES_DATA, FAVORITES_REMOVE, FAVORITES_ADD, FAVORITES_STATUS_UPDATE, FAVORITES_DATA_UPDATED} from 'src/actionTypes';
import Cti from 'src/helpers/Cti';

var dispatch_tmp = (function() {});
const onFavoriteUpdated = (favorites) => {
    dispatch_tmp({
        type: FAVORITES_DATA_UPDATED,
        data: favorites
    });

    dispatch_tmp({
        type: DIRECTORY_DATA_UPDATED,
        data: favorites
    });
}

const onFavorites = (favorites) => {
    dispatch_tmp({
        type: FAVORITES_DATA,
        data: favorites.entries
    });
};

const onPhoneHintStatusEvent = (event) => {
 

    let dispatch_timeout1 = setTimeout(function() {
        dispatch_tmp({
            type: FAVORITES_STATUS_UPDATE,
            data: event
        });
        clearTimeout(dispatch_timeout1);
    }, 5);

    let dispatch_timeout = setTimeout(function() {
        dispatch_tmp({
            type: DIRECTORY_STATUS_UPDATE,
            data: event
        });
        clearTimeout(dispatch_timeout);
    }, 10);
};

export const initFavoritesEvents = () => {
 
    Cti.setHandler(Cti.MessageType.FAVORITEUPDATED, onFavoriteUpdated);
    Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, onPhoneHintStatusEvent);
    return (dispatch) => {dispatch_tmp = dispatch;};
};

export const getFavorites = () => {
 if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.getFavorites();
    Cti.setHandler(Cti.MessageType.FAVORITES, onFavorites);
    return (dispatch) => {dispatch_tmp = dispatch;};
};

export const removeFavorite = (contact_id, source) => {
 if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.removeFavorite(contact_id, source);
    return({
        type: FAVORITES_REMOVE
    });
};

export const addFavorite = (contact_id, source) => {
if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.addFavorite(contact_id, source);
    return({
        type: FAVORITES_ADD
    });
};