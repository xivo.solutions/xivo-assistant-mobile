import {APP_RESUME, APP_CALL_ANDROID, APP_KEYBOARD_OPEN, APP_KEYBOARD_CLOSE, APP_BACKBUTTON} from 'src/actionTypes';
import Cti from 'src/helpers/Cti';
let dispatch_tmp = function() {};


function onKeyboardShow (e) {
	dispatch_tmp({
		type: APP_KEYBOARD_OPEN,
		data: e.keyboardHeight
	});
	dispatch_tmp(reset());
}

function onKeyboardHide () {
	dispatch_tmp({
		type: APP_KEYBOARD_CLOSE,
	});
	dispatch_tmp(reset());
}

function onBackButton () {
	dispatch_tmp({
		type: APP_BACKBUTTON,
	});
	dispatch_tmp(reset());
}

export const triggerBackButton = () => {
	return (dispatch) => {
		dispatch({
			type: APP_BACKBUTTON,
		});
		dispatch(reset());
	};
}


function onCallTrapAndroid (state) {
	dispatch_tmp({
		type: APP_CALL_ANDROID,
		data: state
	});
}

function onAppResume() {
	console.log('RESUME');
	
	dispatch_tmp({
		type: APP_RESUME
	});
}


export const initCordovaEvents = () => {


	if (typeof cordova === 'undefined') {
		return {
			type: ''
		};
	}
	



	return (dispatch) => {
		dispatch_tmp = dispatch;
		window.removeEventListener('native.keyboardshow', onKeyboardShow);
		window.addEventListener('native.keyboardshow', onKeyboardShow);

		window.removeEventListener('native.keyboardhide', onKeyboardHide);
		window.addEventListener('native.keyboardhide', onKeyboardHide);

		document.removeEventListener('backbutton', onBackButton);
		document.addEventListener('backbutton', onBackButton);

		document.addEventListener('resume', onAppResume);

		if (typeof cordova !== 'undefined') {
			PhoneCallTrap.onCall(onCallTrapAndroid);
		}
		
	};
};


const reset = () => {
	window.dispatchEvent(new Event('resize'));
    return {
        type: '',
        message: ''
    };
};