import {CALL_VOICEMAIL, DIRECTORY_RESET, CALL_RESET, USER_RESET, FAVORITES_RESET, USER_LOGGEDON,USER_LOGOUT,USER_DATA, USER_USE_LOCAL_CONTACTS} from 'src/actionTypes';
import {getConstant} from 'src/constants';
import Cti from 'src/helpers/Cti';
import LocalDatas from 'src/LocalDatas';

var login_in_progress = false;
var dispatch_tmp = (function() {});
var tempLogin = {
};

const onLoggedOn = () => {
    login_in_progress = false;
    
    
    Cti.getConfig('line');
    Cti.getList('phone');
    Cti.getConfig('phone');

    dispatch_tmp({
        type: USER_LOGGEDON,
    });
};

const onUserConfigUpdate = (config) => {
    dispatch_tmp({
        type: USER_DATA,
        data: config
    });
};

const onLingConfig = (config) => {
};

const onLinkStatusUpdate = (socket) => {
    if (socket.status === 'error') {
        login_in_progress = false;
    }
};  

const onWebsocketTimeout = () => {
    login_in_progress = false;
};

const onVoiceMailStatusUpdate = (data) => {
    dispatch_tmp({
        type: CALL_VOICEMAIL,
        data
    });
};

export const login = (username, password) => {
    
    if (typeof cordova !== 'undefined') {
        cordova.plugins.Keyboard.close();
    }

    if (login_in_progress) {
        return {
            type: 'NONE'
        };
    }

    login_in_progress = true;
    const WS_CONFIG = getConstant('WS_CONFIG');

    console.log('login', {username, password});

    const wsurl = `${WS_CONFIG.PROTOCOL}://${WS_CONFIG.URL}/xuc/ctichannel?username=${encodeURIComponent(username)}&amp;agentNumber=${WS_CONFIG.DEFAULT_PHONE_NUMBER}&amp;password=${encodeURIComponent(password)}`;

    LocalDatas.set('user_login_info', {username, password});
    LocalDatas.set('user_logged', {username, password});
    tempLogin = {username, password};

    Cti.WebSocket.close();
    
    Cti.WebSocket.init(wsurl, username, WS_CONFIG.DEFAULT_PHONE_NUMBER);
    Cti.debugMsg = false;

    window.Cti = Cti;

    Cti.setHandler(Cti.MessageType.LOGGEDON, onLoggedOn);
    Cti.setHandler(Cti.MessageType.USERCONFIGUPDATE, onUserConfigUpdate);
    Cti.setHandler(Cti.MessageType.LINECONFIG, onLingConfig);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, onLinkStatusUpdate);
    Cti.setHandler(Cti.MessageType.WEBSOCKETTIMEOUT, onWebsocketTimeout);
    Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, onVoiceMailStatusUpdate);
    return (dispatch) => {
        dispatch_tmp = dispatch;
    };
};

export const hangup = () => {
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.hangup();

    return {
        type: 'hangup'
    }
};

export const getFavorites = () => {
  if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    Cti.getFavorites();

    return {
        type: 'getFavorites'
    }
};

export const addFavorite = (phone) => {
    if (typeof Cti.webSocket !== 'undefined' && Cti.webSocket.readyState > 1) {
       return {
            type: 'APP_WEBSOCKET_OFFLINE'
        };
   }

    //Cti.addFavorite('test', phone);
    Cti.addFavorite(phone, 'test');
    return {
        type: 'addFavorite'
    }
};

export const useLocalContacts = (use_local_contacts) => {
    return {
        type: 'USER_USE_LOCAL_CONTACTS',
        use_local_contacts
    }
};

export const isUsingLocalContacts = () => {
    return parseInt(LocalDatas.get('USER_USE_LOCAL_CONTACTS'), 10) > 0;
};

window.isUsingLocalContacts = isUsingLocalContacts;

export const resetContext = () => {
    login_in_progress = false;
    return (dispatch) => {
        dispatch({type: USER_LOGOUT});
        dispatch({type: DIRECTORY_RESET});
        dispatch({type: FAVORITES_RESET});
        dispatch({type: CALL_RESET});
        dispatch({type: USER_RESET});
    };
};

export const logout = () => {
    login_in_progress = false;
    Cti.WebSocket.close();
    LocalDatas.remove('user_logged');

    return (dispatch) => {
        dispatch({type: USER_LOGOUT});
        dispatch({type: DIRECTORY_RESET});
        dispatch({type: FAVORITES_RESET});
        dispatch({type: CALL_RESET});
        dispatch({type: USER_RESET});
    };
};
