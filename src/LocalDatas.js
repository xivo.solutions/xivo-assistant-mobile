

class LocalData {
	constructor() {
		this.prefix = 'XivoM_';

	}

	initStorage(key, data) {
		
		let stored_data = localStorage.getItem(`${this.prefix}_${key}`);
		if (!stored_data) {
			stored_data = data
			localStorage.setItem(`${this.prefix}_${key}`, JSON.stringify(data));
		} else {
			stored_data = JSON.parse(stored_data);
		}
		
		return stored_data;
	}


	get(key) {
		let data = localStorage.getItem(`${this.prefix}_${key}`);

		if (typeof data !== 'undefined') {
			return JSON.parse(data);
		}

		return false;
	}

	set(key, data) {
		localStorage.setItem(`${this.prefix}_${key}`, JSON.stringify(data));
		return data;
	}

	remove(key) {
		localStorage.removeItem(`${this.prefix}_${key}`)
	}
};



export default new LocalData();