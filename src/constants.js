import LocalDatas from 'src/LocalDatas';
import package_file from '../package.json';

export const SWIPE_LEFT = 2;
export const SWIPE_RIGHT = 4;

export const AUTH_VIEWS = {
    LOGIN: 'LOGIN',
    INPROGRESS: 'INPROGRESS',
    TIMEOUT: 'TIMEOUT',
    LOGINERROR: 'LOGINERROR',
    OPTIONS: 'OPTIONS',
    LOADING: 'LOADING' 
}

export const STATUS = {
    RINGING: 'RINGING',
    AVAILABLE: 'AVAILABLE',
    OFFLINE: 'OFFLINE',
    CALLING: 'CALLING',
    HUNGUP: 'HUNGUP',
    UP: 'UP'
};

export const PHONE_SELECTOR = {
	MOBILE: 'mobile', 
	FORWARD: 'forward'
};

export const PHONE_STATUS_COLOR = {


    "16" : "#F7FE2E",
    "8" : "#2E2EFE",

    "9" : "#CC2EFA",
    "1" : "#e67d39",
    "2" : "#81BEF7",

    "0" : "#2ba41b",    
    
    "4" : "#959595",
    "-1" : "#959595",
    "-2" : "#959595",
    "-99" : "#959595"
};

export const VERSION = package_file.version;

let saved_ws_config = {};
let consts = {
	DEFAULT_LANG: 'fr',
	WS_CONFIG: {
	    URL: '',
	    PROTOCOL: 'wss',
	    DEFAULT_PHONE_NUMBER: '1000'
	},
	USER_USE_LOCAL_CONTACTS: '0',
	VOICEMAIL_NUMBER: '*98',
	FWD: {
		enabled: false,
		destination: ''
	}
}

window.saveConstant = function (key, data) {
    saveConstant(key, data);
};

export const getConstant = (key) => {
	let local_consts = LocalDatas.get('consts') || {};
	consts = {...consts, ...local_consts};
	return consts[key];
}

export const saveConstant = (key, data) => {

    if (typeof data === 'object') {
        consts[key] = {...consts[key], ...data};
    } else {
        consts[key] = data;
    }


	LocalDatas.set('consts', consts);
};
