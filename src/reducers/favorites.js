import {FAVORITES_RESET, FAVORITES_DATA, FAVORITES_STATUS_UPDATE, FAVORITES_DATA_UPDATED} from 'src/actionTypes';
import {getFavorites} from 'src/actions/favorites';
var update = require('react-addons-update');
const default_state = {
	loaded: false,
	data: []
};
function favorites(state = default_state, action) {
    let {data} = state;
    let index = -1;
    switch (action.type) {
        case FAVORITES_DATA:
            return {...state, data: action.data, loaded: true};
        case FAVORITES_STATUS_UPDATE:
            index = data.findIndex((item) => (item.entry[1] === action.data.number));

            if (index !== -1) {
                data = update(state.data, {[index]: {status: {$set: action.data.status}}});
            }

        	return index !== -1 ? {...state, data: data} : state;

        case FAVORITES_DATA_UPDATED:
            index = data.findIndex((item) => (item.contact_id === action.data.contact_id));

            if (action.data.action === 'Removed' && index !== -1) {
                data = update(data, {$splice: [[index, 1]]});
            }

            
            return {...state, data: data};
        case FAVORITES_RESET:
            return default_state;
        default:
            return state;
    }
}

export default favorites;