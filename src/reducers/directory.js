import {DIRECTORY_STATUS_UPDATE, DIRECTORY_DATA_UPDATED, DIRECTORY_XIVO_DATA, DIRECTORY_LOCAL_DATA, DIRECTORY_RESET, DIRECTORY_RESET_LOAD} from 'src/actionTypes';
var update = require('react-addons-update');

const default_state = {
    xivo: [],
    local: [],
    xivo_loaded: false,
    local_loaded: false,
    search: ''
};
function directory(state = default_state, action) {
    let {xivo} = state;
    switch (action.type) {
        case DIRECTORY_XIVO_DATA:
            return {...state, xivo: [...action.data], xivo_loaded: true, search: action.search};
        case DIRECTORY_LOCAL_DATA:
            return {...state, local: [...action.data], local_loaded: true, search: action.search};
        case DIRECTORY_RESET_LOAD: 
            return {...state, xivo_loaded: false, local_loaded:false, search: ''};
        case DIRECTORY_RESET:
            return default_state;
        case DIRECTORY_DATA_UPDATED: 
            let index = xivo.findIndex((item) => (item.contact_id === action.data.contact_id));
            let is_favorite = false;
            let action_name = action.data.action;

            if (index !== -1) {

                if (action_name === 'Added' || action_name === 'RemoveFail') {
                    is_favorite = true;
                } else if (action_name === 'Removed' || action_name === 'AddFail') {
                    is_favorite = false;
                }

                xivo = update(xivo, {[index]: {favorite: {$set: is_favorite}}});
            }
            return {...state, xivo};
        case DIRECTORY_STATUS_UPDATE: 
             index = -1
            if (typeof item !== 'undefined' && item !== null && typeof item.entry !== 'undefined' && item.entry.length > 0) {
                index = xivo.findIndex((item) => (item.entry[1] === action.data.number));
            }

            

            if (index !== -1) {
                xivo = update(state.xivo, {[index]: {status: {$set: action.data.status}}});
            }

            return index !== -1 ? {...state, xivo} : state;
        default:
            return state;
    }
}

export default directory;
