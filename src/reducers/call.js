import {CALL_CALLING, CALL_RESET, CALL_HISTORY, CALL_STATUS, CALL_UPDATE, CALL_VOICEMAIL, CALL_DIAL, CALL_END_DIAL} from 'src/actionTypes';
var update = require('react-addons-update');
import {STATUS} from 'src/constants';

const default_state = {
    history: {
        loaded: false,
        data: []
    },
    status: STATUS.OFFLINE,
    event: {},
    voicemail: {},
    current_call: {},
    calling: false
};
function user(state = default_state, action) {
    switch (action.type) {
        case CALL_HISTORY:
            var obj = state.history.data;
            var newObj = update(obj, {$merge: action.data});
            return {...state, history: {loaded: true, data: newObj}};
        case CALL_STATUS:
            return {...state, ...action.status};
        case CALL_UPDATE:
            return {...state, event: action.event};
        case CALL_VOICEMAIL:
            return {...state, voicemail: action.data};
        case CALL_CALLING: 
            return {...state, calling: action.calling};
        case CALL_DIAL:
            return {...state, current_call: {
                number: action.number,
                name: action.name
            }};
        case CALL_END_DIAL:
            return {...state, current_call: {}};
        case CALL_RESET:
            return default_state;
        default:
            return state;
    }
}

export default user;
