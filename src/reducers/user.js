import {USER_RESET, USER_LOGGEDON, USER_LOGOUT, USER_LOGIN_ERROR, USER_DATA, USER_USE_LOCAL_CONTACTS} from 'src/actionTypes';
import LocalDatas from 'src/LocalDatas';
import * as Consts from 'src/constants';

const default_state = {
    logged: false,
    loaded: false,
    local_user: LocalDatas.get('user_logged'),
    local_user_info: LocalDatas.get('user_login_info'),
    use_local_contacts: isUsingLocalContacts(),
    data: {}
};
function user(state = default_state, action) { 
    switch (action.type) {
        case USER_LOGGEDON:
            return {
                ...state, 
                logged: true, 
                local_user: LocalDatas.get('user_logged'),
                local_user_info: LocalDatas.get('user_login_info')
            };
        case USER_DATA:
            return {...state, ...removeNullValueFromObject(action.data), loaded: true};
        case USER_LOGOUT:
            console.log(USER_LOGOUT, default_state);
            return {...default_state, local_user: {username: '', password: ''}, data: {}, local_user: LocalDatas.get('user_logged'),local_user_info: LocalDatas.get('user_login_info'), use_local_contacts: isUsingLocalContacts()};
        case USER_USE_LOCAL_CONTACTS: 
            console.log('USER_USE_LOCAL_CONTACTS');
            let use_local = action.use_local_contacts;
            console.error('USER_USE_LOCAL_CONTACTS', use_local);
             LocalDatas.set('USER_USE_LOCAL_CONTACTS', use_local ? '1' : '0');
            return {...state, use_local_contacts: use_local}
        case USER_RESET: 
            return {...default_state, use_local_contacts: isUsingLocalContacts()};
        default:
            return state;
    }
}


function removeNullValueFromObject(data) {
    let temp = data;
    for(var i in temp) {
        if (temp[i] === null) {
            delete temp[i];
        } else {
            if (temp[i] === 'null') {
                temp[i] = '';
            }
        }
    }

    return temp;
}

export default user;