
const default_state = {
    event: '',
    data: false
};
function app(state = default_state, action) {
    let new_state = {...state, event: action.type};
    if (typeof action.data !== 'undefined') {
        new_state.data = action.data;
    }

    return new_state;
}

export default app;