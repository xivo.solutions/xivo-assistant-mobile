import React, {Component} from 'react';
import { connect } from 'react-redux';

import {initCordovaEvents} from 'src/actions/app';
import DevTools from 'containers/tools/DevTools';

class Viewport extends Component{
    constructor() {
        super();
        this.state = {
            'viewport:size': {
                width: window.innerWidth,
                height: window.innerHeight
            }
        };

    }

    componentWillMount() {
        let _this = this;
        window.addEventListener('resize', function() {
            _this.setState({'viewport:size': {
                width: window.innerWidth,
                height: window.innerHeight
            }});
        }, true);

        window.addEventListener('deviceready', function() {
            _this.setState({'viewport:size': {
                width: window.innerWidth,
                height: window.innerHeight
            }});
        }, true);
    }

    componentDidMount() {
        this.props.initCordovaEvents();
    }


    render() {
        let styles = {
            overflow: 'hidden',
            ...this.state['viewport:size']
        };

        /*var childrenWithProps = React.Children.map(this.props.children, function(child) {
            return React.cloneElement(child);
        });*/

        return (
            <div style={styles}>
                {this.props.children}

            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {};
};

const ViewportContainer = connect(
    mapStateToProps,
    {
        initCordovaEvents
    }
)(Viewport);

export default ViewportContainer;