import React, {Component} from 'react';
import { connect } from 'react-redux';
import SpriteAnimator from 'react-sprite-animator';

import i18n from 'src/i18n';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'src/components/Button';

import style from 'scss/auth.scss';

class InProgressComponent extends Component {
    render() {
        return (
            <div className={style.auth}> 
                <header>
                    <SVGIcon filename={'connexion_logo'} width={240}/>
                </header>

                <div className={style.container}>
                    <div className={style.wrapper}>
                        <SpriteAnimator
                        sprite={require('assets/svg/loader_anim.svg')}
                        width={75}
                        height={75}
                        direction={'horizontal'}
                        fps={10} 
                        />

                        <p style={{marginBottom: 30}}>{i18n.translate('SERVER_CONNECTING')}</p>
                    </div>
                    <Button type="cancel" onClick={this.props.onCancelAction.bind(this)}>Annuler</Button>

                </div>
                
            </div>
        );
    }

}


export default InProgressComponent;
