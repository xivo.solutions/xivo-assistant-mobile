import React, {Component} from 'react';
import { connect } from 'react-redux';
import style from 'scss/auth.scss';
import classNames from 'classnames';


import i18n from 'src/i18n';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'src/components/Button';
import {saveConstant, getConstant} from 'src/constants';

class OptionsComponent extends Component {

    static defaultProps = {
        hasCancelBtn: false
    };

    static contextTypes = {
        router: React.PropTypes.object
    };

    constructor() {
        super();

        let {PROTOCOL} = getConstant('WS_CONFIG');

        this.state = {
            normal: (PROTOCOL === 'ws')
        };
    }

    radioChange(e) {
        let value = parseInt(e.currentTarget.value, 10);
        this.setState({
            normal: value < 1
        });
    }

    onOptionsFormSubmitted(e) {
        e.preventDefault();
        e.stopPropagation();

        let {server, protocol} = this.refs.options_form.elements;
        let formatted_protocol = parseInt(protocol.value, 10) > 0 ? 'wss' : 'ws'; 
        if (typeof server !== 'undefined' && typeof protocol !== 'undefined'){
            saveConstant('WS_CONFIG', {URL: server.value, PROTOCOL: formatted_protocol});
            this.props.onSubmitOptionsAction();
        }
       
        //console.log('form:update', this.refs.options_form.elements);
        return false;
    }

    render() {
        let {hasCancelBtn} = this.props;
        let type_normal = classNames({
            [`${style.active}`]: this.state.normal
        });

        let type_secure = classNames({
            [`${style.active}`]: !this.state.normal
        });

        let {URL} = getConstant('WS_CONFIG');

        return (
            <div className={style.auth}>
                <header className={style.option_header}>

                    <span className={style.option_btn}>
                        <SVGIcon filename={'connexion_options_avancees'} width={50} />
                        <i className={'fa fa-cog'} /> 
                    </span>
                    <h3>{i18n.translate('OPTIONS_TITLE')}</h3>
                </header>
                <div className={style.container}>
                    <form ref={'options_form'} className={style.options_form} onSubmit={this.onOptionsFormSubmitted.bind(this)}>
                        <div className={style.type_server}>
                            <h4>TYPE DE SERVEUR</h4>
                            <ul>
                                <li className={type_normal}>
                                    <label htmlFor={'type_normal'}>
                                        <span>accès non sécurisé (WS://)</span>
                                        {this.state.normal ?  <SVGIcon filename='check' width={17}/> : null}
                                    </label>
                                    
                                </li>
                                <li className={type_secure}>
                                    <label htmlFor={'type_secure'}>
                                        <span>accès sécurisé (WSS://)</span>
                                         {!this.state.normal ?  <SVGIcon filename='check' width={17}/> : null}
                                    </label>
                                   
                                </li>
                            </ul>

                            <div style={{display: 'none'}}>
                                <input onChange={this.radioChange.bind(this)} type="radio" name="protocol" id={'type_normal'} checked={this.state.normal ? 'checked': ''} defaultValue="0" />
                                <input onChange={this.radioChange.bind(this)} type="radio" name="protocol" id={'type_secure'} checked={!this.state.normal ? 'checked': ''} defaultValue="1" />
                            </div>
                        </div>
                        <div className={style.url_server}>
                            <h4>URL DU SERVEUR</h4>
                            <input type="text" id="server" defaultValue={URL} placeholder={i18n.translate('SERVER')}/>
                        </div>

                        
                        { hasCancelBtn ? (
                            <div className={style.two_btns}>
                                <Button onClick={this.onOptionsFormSubmitted.bind(this)}>{i18n.translate('VALIDATE')}</Button>
                                <Button type={'cancel'} onClick={this.onOptionsFormSubmitted.bind(this)}>{i18n.translate('CANCEL')}</Button>
                            </div>
                        ) : (<Button onClick={this.onOptionsFormSubmitted.bind(this)}>{i18n.translate('VALIDATE')}</Button>)}

                        
                    </form>
                </div>
            </div>
        );
    }

}


export default OptionsComponent;