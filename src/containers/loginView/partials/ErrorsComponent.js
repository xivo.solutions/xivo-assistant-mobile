import React, {Component} from 'react';
import { connect } from 'react-redux';
import style from 'scss/auth.scss';
import classNames from 'classnames';
import i18n from 'src/i18n';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'src/components/Button';


class ErrorsComponent extends Component {

    static defaultProps = {
        errorText: 'SERVER_TIMEOUT'
    };

    render() {
        let {errorText} = this.props;
        return (
            <div className={classNames(style.auth, style.error_auth)}> 
                <header>
                    <SVGIcon filename={'connexion_logo'} width={240}/>
                </header>
                <div className={classNames(style.container, style.error_wrapper)}>
                    <p dangerouslySetInnerHTML={{__html: i18n.translate(errorText)}} />
                    <Button type="cancel" onClick={this.props.onCancelAction.bind(this)}>Annuler</Button>
                </div>
                
            </div>
        );
    }

}


export default ErrorsComponent;
