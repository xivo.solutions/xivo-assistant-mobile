import { connect } from 'react-redux';
import { login, logout, hangup, getFavorites,addFavorite } from 'src/actions/user';
import React, { Component } from 'react';
import LocalDatas from 'src/LocalDatas';
import {saveConstant, getConstant} from 'src/constants';
import Modal from 'src/components/Modal';
import Button from 'src/components/Button';
import ScrollView from 'src/components/ScrollView';
import Cti from 'src/helpers/Cti';


//TODO REMOVE JQUERY
import $ from 'jquery';

import i18n from 'src/i18n';
import SVGIcon from 'components/tools/SVGIcon';

import style from 'scss/login.scss';

class Login extends Component {

    constructor() {
        super();
        this.state = {
            advanced_options: false,
            modal: {
                hide: true,
                msg: ''
            }
        };
    }

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        this.props.onLogin(e.currentTarget.elements);
        return false;
    }

    componentWillReceiveProps(nextProps) {

    }

    componentDidMount() {
       let {user, login} = this.props;
        if (user.local_user && user.local_user.username && user.local_user.password) {
            //login(user.local_user.username, user.local_user.password);
        }
    }


    onClickAdvancedOptions(e) {
        e.preventDefault();
        e.stopPropagation();

        this.props.onAdvancedOptions();

        return false;
    }

    onCancelModalBtn() {
        this.setState({
            modal: {
                hide: true,
                msg: ''
            }
        });
    }

    renderOptions() {

        if (!this.state.advanced_options) {
            return null;
        }


        let ws_config = getConstant('WS_CONFIG');

        return(
            <div className={style.options_container}>
                <div>
                    <label htmlFor={'protocol'}>
                        <span>
                            Type de serveur : 
                        </span>
                        <select id={'protocol'} name={'protocol'} defaultValue={ws_config.PROTOCOL}>
                            <option value={'ws'}>WS://</option>
                            <option value={'wss'}>WSS://</option>
                        </select>
                    </label>
                </div>
                <div className={style.options_server}>
                    <label htmlFor={'server'}>
                            URL du serveur : 
                    </label>
                    <input type="text" id="server" defaultValue={ws_config.URL} placeholder={i18n.translate('SERVER')}/>
                </div>
            </div>   
        );
    }

    render() {


        let local_user_info = LocalDatas.get('user_login_info');
        let previous_username = local_user_info ? local_user_info.username : '';
        let previous_password = local_user_info ? local_user_info.password : '';
        
        return(
            <div className={style.container}>

                <Modal hide={this.state.modal.hide}>
                    <p>{this.state.modal.msg}</p>
                    <Button type="cancel" onClick={this.onCancelModalBtn.bind(this)}>Annuler</Button>
                </Modal>
                <ScrollView className={style.login_container}>
                    <header>
                        <SVGIcon filename={'connexion_logo'} width={240}/>
                    </header>
                    <section>
                        <p className={style.login_text}>{i18n.translate('LOGIN_TEXT')}</p>
                        <form id={'login_form'} action={''} onSubmit={this.onSubmit.bind(this)} style={{textAlign: 'center'}} className={style.form_elements}>
                            <SVGIcon filename={'connexion'} width={38}/>
                            <h4 style={{marginTop: 10, marginBottom: 15}}>{i18n.translate('SIGN_IN')}</h4>
                            <p>
                                <input type="text" defaultValue={previous_username} autoComplete="off" autoCorrect="off" autoCapitalize="off" id="login" placeholder={i18n.translate('LOGIN')}/>
                            </p>
                            <p>
                                <input type="password" defaultValue={previous_password} autoComplete="off" autoCorrect="off" autoCapitalize="off" id="password" placeholder={i18n.translate('PASSWORD')}/>
                            </p>
                            <p>
                                <a href={''} onClick={this.onClickAdvancedOptions.bind(this)} className={style.advanced_options}>
                                    <i className={'fa fa-cog'} /> 
                                    <span>Options avancées</span>
                                </a>
                            </p>
                                {this.renderOptions()}
                            <p>
                                <button id={'login_form_submit'} type="submit">{i18n.translate('SIGN_IN_ME')}</button>
                            </p>

                        </form>
                    </section>
                </ScrollView>
            </div>
        );
    }
}

Login.contextTypes = {
    router: React.PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

const LoginContainer = connect(
    mapStateToProps,
    {
        login,
        logout,
        hangup,
        getFavorites,
        addFavorite
    }
)(Login);

export default LoginContainer;
