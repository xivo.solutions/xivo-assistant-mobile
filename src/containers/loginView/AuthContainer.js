import React, {Component} from 'react';
import { connect } from 'react-redux';
import SpriteAnimator from 'react-sprite-animator';
import ScrollView from 'src/components/ScrollView';
import { login, logout, resetContext } from 'src/actions/user';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'src/components/Button';
import {AUTH_VIEWS} from 'src/constants';
import Cti from 'src/helpers/Cti';
import i18n from 'src/i18n';

import LoginContainer from 'containers/loginView/LoginContainer';
import InProgressComponent from 'containers/loginView/partials/InProgressComponent';
import OptionsComponent from 'containers/loginView/partials/OptionsComponent';
import ErrorsComponent from 'containers/loginView/partials/ErrorsComponent';
import LoadingComponent from 'containers/loginView/partials/LoadingComponent';

import LocalDatas from 'src/LocalDatas';

import style from 'scss/auth.scss';

class AuthContainer extends Component {

    constructor() {
        super();
        this.state = {
            view: AUTH_VIEWS.LOADING
        };
        this.timeout_view = null;
        this.force_close = false;
    }

    componentDidMount() {
        let _this = this;
        let {login, user} = this.props;
        let timer = 2000;

       
        if (this.userIsLogged()) {

            let {username, password} = LocalDatas.get('user_logged');;
            this.timeout_view = setTimeout(function() {

                if (!navigator.onLine) {
                    _this.setState({
                        view: AUTH_VIEWS.TIMEOUT
                    });
                    return false;
                }
                
                login(username, password);
                _this.setState({
                    view: AUTH_VIEWS.INPROGRESS
                });
            }, timer);
        } else {
            
            this.timeout_view = setTimeout(function() {
                _this.setState({
                    view: AUTH_VIEWS.LOGIN
                });
            }, timer);
        }
        Cti.setHandler(Cti.MessageType.WEBSOCKETTIMEOUT, this.webSocketTimeout.bind(this));
    }


    componentWillUpdate() {
    }

    componentWillReceiveProps(nextProps) {
        let _this = this;
        
        setTimeout(function(){
            if (nextProps.user.logged) {
                
                _this.context.router.push('/history');
            }
        }, 2000);

        if (typeof cordova !== 'undefined' && nextProps.app.event === 'APP_RESUME' && Cti.WebSocket.getSocket().readyState > 1) {
             let _this = this;
             let reactTimeout = setTimeout(function() {
                _this.context.router.push('/login');
                Cti.WebSocket.close();
                _this.props.resetContext();
                clearTimeout(reactTimeout);
             }, 250);
             
             console.log('APP:RESET-RESUME', Cti.webSocket);
             return false;
         }
         
         if (nextProps.app.event === 'APP_WEBSOCKET_OFFLINE') {
             //this.context.router.push('/login');
             // -> Maybe need a timeout fro correctly change the state
             let _this = this;
              let reactTimeout = setTimeout(function() {
                _this.context.router.push('/login');
                Cti.WebSocket.close();
                _this.props.resetContext();
                clearTimeout(reactTimeout);
             }, 250);

             console.log('APP_WEBSOCKET_OFFLINE', Cti.webSocket)
         }

    }



    webSocketTimeout(socket) {
        let view = AUTH_VIEWS.TIMEOUT;

        if (this.force_close === true) {
            this.force_close = false;
            return false;
        }
        console.log('webSocketTimeout', socket.status);
        if (socket.status === 'error') {
            view = AUTH_VIEWS.LOGINERROR;
            clearTimeout(Cti.WebSocket.getTimeout());
        }

        this.setState({view});
    }

    userIsLogged() {
        let {user} = this.props;
        let local_user = LocalDatas.get('user_logged');
        let test = (local_user !== null && Object.keys(local_user).length > 0);
        return test;
    }

    onLogin(formElements) {
        //Display -> login en cours // Laisse le fonctionnement initial
        

        this.force_close = false;
        let {login, password, server, protocol} = formElements;

        if (!navigator.onLine) {
            this.setState({
                view: AUTH_VIEWS.TIMEOUT
            });
            return false;
        }


        if (this.state.advanced_options && typeof server !== 'undefined' && typeof protocol !== 'undefined'){
            saveConstant('WS_CONFIG', {URL: server.value, PROTOCOL: protocol.value});
        }

        console.log('onLogin', login.value, password.value);


        this.props.login(login.value, password.value);
        this.setState({
            view: AUTH_VIEWS.INPROGRESS
        });
    }

    onAdvancedOptions() {
        this.setState({
            view: AUTH_VIEWS.OPTIONS
        });
    }

    onSubmitOptionsAction() {
        this.onCancelAction();
    }

    onCancelAction() {
        LocalDatas.remove('user_logged');
        this.setState({
            view: AUTH_VIEWS.LOGIN
        });

        //this.props.resetContext();

        try {
            this.force_close = true;
            clearTimeout(Cti.WebSocket.getTimeout());
        } catch (error) {
            console.log('Cannot clear Timeout !')
        }

        this.props.logout();
    }

    displayAppropriateView(state) {
        //TODO: mettre les test de comparaison dans les constants

        let error_component = (<ErrorsComponent onCancelAction={this.onCancelAction.bind(this)} />);
        console.log('displayAppropriateView', state);
        let view = '';
        if (state === AUTH_VIEWS.LOGIN){
            view = (<LoginContainer onLogin={this.onLogin.bind(this)} onAdvancedOptions={this.onAdvancedOptions.bind(this)} />);
        }else if (state === AUTH_VIEWS.INPROGRESS) { //Connexion en cours
            view = (<InProgressComponent onCancelAction={this.onCancelAction.bind(this)} />);
        } else if (state === AUTH_VIEWS.OPTIONS) { //Timeout websocket
            view = (<OptionsComponent onSubmitOptionsAction={this.onSubmitOptionsAction.bind(this)} />);
        } else if (state === AUTH_VIEWS.TIMEOUT) { //Timeout websocket
            view = error_component;
        } else if (state === AUTH_VIEWS.LOGINERROR) { //Loginerror
            view = <ErrorsComponent onCancelAction={this.onCancelAction.bind(this)} errorText={'SERVER_LOGIN_ERROR'}/>; 
        } else if (state === AUTH_VIEWS.LOADING) { //Loginerror
            view = (<LoadingComponent />);
        }else { //Impossible all other reasons
            view = error_component;
        }


        return view;
    }


    render() {
        return (
            <ScrollView>
               {this.displayAppropriateView(this.state.view)}
            </ScrollView>
        );
    }

}


AuthContainer.contextTypes = {
    router: React.PropTypes.object
};

export default connect(
    (state) => ({
        local_user: state.user.local_user,
        user: state.user,
        app: state.app
    }), //mapStateToProps
    {
        login,
        logout,
        resetContext
    } //mapActionToProps
)(AuthContainer);
