import { connect } from 'react-redux';
import { search, resetDirectory } from 'src/actions/directory';
import { addFavorite, removeFavorite, initFavoritesEvents } from 'src/actions/favorites';
import { dial } from 'src/actions/call';
import React, { Component } from 'react';
import style from 'scss/call_history.scss';
import DirectoryItem from 'components/Directory/Item';
import DirectoryLocalItem from 'components/Directory/ItemLocal';
import ScrollView from 'components/ScrollView';
import Modal from 'components/Modal';
import Button from 'components/Button';
import PhoneNumberSelection from 'components/PhoneNumberSelection';

import i18n from 'src/i18n';

import classNames from 'classnames';


class DirectoryListing extends Component {
    constructor() {
        super();
        this.state = {
            to_call: {}
        };

        this.xivo_list = [];
        this.local_list = [];
    }

    componentWillMount() {
        this.props.resetDirectory();
        this.props.initFavoritesEvents();
    }

    onFavoriteButton(contact_id, source, is_favorite) {
        if (is_favorite) {
            this.props.removeFavorite(contact_id, source);
        } else {
            this.props.addFavorite(contact_id, source);
        }
    }

    onItemClick(item, type) {
        let {displayModal} = this.props;
        let phone_numbers = [];
        let username = '';

        if (type === 'local') {
            username = item.name.formatted;
            for (let i in item.phoneNumbers) {
                let number = item.phoneNumbers[i];
                if (number.value.length > 0) {
                    phone_numbers.push(number.value);
                }
            }
         
        } else {
            username = item.entry[0];
            for (let i = 1, l = 4; i < l; i++) {
                let entry = item.entry[i];

                if (entry.length > 0) {
                    phone_numbers.push(item.entry[i]);
                }
            }
        }


        if (phone_numbers.length > 1) { //Display Modal phone number selection
            displayModal(true, <PhoneNumberSelection onDial={this.onDialBtn.bind(this)} onClose={() => {displayModal(false, null)}} numbers={phone_numbers} username={username} />);
        } else {
            if (phone_numbers.length > 0) {
                this.onDialBtn(phone_numbers[0], username);
            } else {
                displayModal(true, (
                    <div>
                        <p>
                            Ce contact n'a pas de numéro de téléphone
                        </p>
                        <Button type="cancel" onClick={() => {displayModal(false, null);}}>{i18n.translate('OK')}</Button>
                    </div>
                ));
            }
        }



    }

    onDialBtn(destination, name) { 
        let {dial, displayModal, user} = this.props;
        let from_mobile = (user.mobileNumber && user.mobileNumber.length > 0);
        displayModal(false, null);
        dial(destination, name, from_mobile);
        this.context.router.push('/incall');
    }

    onCloseDial(e) {
        this.props.displayModal(false, null);
    }

    renderModal(to_call) {
        return (
            <div>
                {(typeof to_call.name !== 'undefined') ? (
                    <div>
                        <p>{i18n.translate('NUMBER_CALL_TXT')}</p>
                        <p>
                             {to_call.name}
                        </p>
                        <Button onClick={this.onDialBtn.bind(this, to_call.phone_number, to_call.name)}>{i18n.translate('CALL')}</Button>
                    </div>

                    ) : (
                    <p>{i18n.translate('UNKNOWN_CALL_IMPOSSIBLE')}</p>
                )}

                <Button type="cancel" onClick={this.onCloseDial.bind(this)}>{i18n.translate('CANCEL')}</Button>
            </div>
        );
    }

    componentWillReceiveProps(nextProps) {
        let {directory, favorites} = nextProps;
        let _this = this;
        
        if (directory.local_loaded) {


            this.local_list = directory.local.map(function(directory_item, index) {
                console.log('directory:iteration', directory_item.name.formatted);
                return <DirectoryLocalItem key={index + '_local'} item={directory_item} onClickItem={_this.onItemClick.bind(_this, directory_item, 'local')} />;
            });
        }


        if (directory.xivo_loaded) {
            this.xivo_list = directory.xivo.map(function(directory_item, index) {
               return <DirectoryItem key={index + '_xivo'} item={directory_item} onClickItem={_this.onItemClick.bind(_this, directory_item, 'xivo')} onFavoriteButton={_this.onFavoriteButton.bind(_this)} />;
            });   
        }
    }

    renderLoading() {
        return (
            <ScrollView>
                <div className={'loading'}>
                    <h1>{i18n.translate('SEARCH')}</h1>
                    <p>{i18n.translate('LOADING')}</p>
                </div>
            </ScrollView>
        );
    }

    render() {

        let {directory, favorites, user} = this.props;
        let _this = this;


        let xivo_list = (<div key='xivo' className={style.nothing}>Aucun contact dans XiVO</div>);
        let local_list = (<div key='local' className={style.nothing}>Aucun contact en local</div>);

        let display_list_order = [];


        if (directory.local_loaded && directory.xivo_loaded) {

            if (this.xivo_list.length > 0) {
                xivo_list = this.xivo_list;
            } 



            if (user.use_local_contacts) {
                if (this.local_list.length > 0) {
                    local_list = this.local_list;
                }
            } else {
                local_list = (null);
                if (this.xivo_list.length < 1) {
                    var directory_item = {
                        phoneNumbers: [{
                            value: directory.search
                        }],
                        name: {
                            formatted: directory.search
                        }
                    };
                    xivo_list = (<DirectoryLocalItem key={'xivo'} item={directory_item} onClickItem={_this.onItemClick.bind(_this, directory_item, 'local')} />);
                }
            }


            if (user.use_local_contacts && this.xivo_list.length < 1 && this.local_list.length < 1) {
                var directory_item = {
                    phoneNumbers: [{
                        value: directory.search
                    }],
                    name: {
                        formatted: directory.search
                    }
                };
                local_list = (null);
                xivo_list = (<DirectoryLocalItem key={'xivo'} item={directory_item} onClickItem={_this.onItemClick.bind(_this, directory_item, 'local')} />);
            }


            display_list_order = [xivo_list, local_list];
            if (this.xivo_list.length < 1 || this.local_list.length > 0) {
                display_list_order = [local_list, xivo_list];
            }


        } else {
            return this.renderLoading();
        }

        


        return (
            <ScrollView>
                {display_list_order}
            </ScrollView>
        );
    }
}

DirectoryListing.contextTypes = {
    router: React.PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        directory: state.directory,
    };
};

const DirectoryListingContainer = connect(
    mapStateToProps,
    {
        addFavorite,
        removeFavorite,
        initFavoritesEvents,
        dial,
        resetDirectory
    }
)(DirectoryListing);

export default DirectoryListingContainer;
