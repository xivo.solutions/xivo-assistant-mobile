import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { getHistory, dial } from 'src/actions/call';
import classNames from 'classnames';
import i18n from 'src/i18n';
import ScrollView from 'components/ScrollView';
import Modal from 'components/Modal';
import Button from 'components/Button';
import CallHistoryItem from 'components/CallHistory/Item';
import style from 'scss/call_history.scss';

class CallHistory extends Component {
    constructor() {
        super();
        this.state = {
            to_call: {}
        };
    }

    componentWillMount() {
        this.props.getHistory();
    }

    onItemClick(to_call) {
        //this.props.displayModal(true, this.renderModal(to_call));

        let {dial, user} = this.props;
        let from_mobile = (user && typeof user.mobileNumber !== 'undefined' && user.mobileNumber.length > 0);
        dial(to_call.dstNum, null, from_mobile);
        this.context.router.push('/incall');
    }


    onDialBtn(destination, name, e) { 
        this.onCloseDial();
        let {dial, user} = this.props;
        let from_mobile = (user && typeof user.mobileNumber !== 'undefined' && user.mobileNumber.length > 0);
        dial(destination, name, from_mobile);
        this.context.router.push('/incall');
    }

    onCloseDial() {
        this.props.displayModal(false, null);
    }

    renderModal(to_call) {
        return (
            <div>
                {(to_call.dstNum !== null) ? (
                        <div>
                        
                            <p>{i18n.translate('NUMBER_CALL_TXT')}</p>
                            <p>
                                {to_call.dstNum}
                            </p>

                            <Button onClick={this.onDialBtn.bind(this, to_call.dstNum, null)}>{i18n.translate('CALL')}</Button>
                        </div>
                        
                    ) : (
                        <p>{i18n.translate('UNKNOWN_CALL_IMPOSSIBLE')}</p>
                    )}
                    
                    <Button type="cancel" onClick={this.onCloseDial.bind(this)}>{i18n.translate('CANCEL')}</Button>
            </div>
        );
    }

    renderLoading() {
        return (
            <ScrollView>
                <div className={'loading'}>
                    <h1>{i18n.translate('CALL_HISTORY')}</h1>
                    <p>{i18n.translate('LOADING')}</p>
                </div>
            </ScrollView>
        );
    }

    render() {

        let _this = this;
        let {call} = this.props;
        let list = (<div className={style.nothing}>{i18n.translate('CALL_HISTORY_EMPTY')}</div>);
        list = []

        if (!call.history.loaded) {
            return this.renderLoading();
        }

        if (call.history.data.length > 0) {
            let {data} = call.history;
            let date_list = {};
            //TODO Externaliser ce traitement dans le action 
            for(let i in data) {
                let item = data[i];
                let date_start = moment(item.start).format('YYYY-MM-DD');
                if (Object.keys(date_list).indexOf(date_start) === -1) {
                    date_list[date_start] = [item];
                } else {
                    date_list[date_start].push(item);
                }
            }
            
            let last_index = 0;
            for(let index in date_list) {
                let block_data = date_list[index];
                let block_date_list = [];
                for(let index in block_data) {
                    let history_item = block_data[index];
                    block_date_list.push(<CallHistoryItem key={index} item={history_item} onItemClick={_this.onItemClick.bind(_this, history_item)} />);
                }

                let moment_date = moment(index);
                let separator_text = moment_date.isSame(moment(), 'day') ? i18n.translate('MOMENT_HISTORY_NOW') : moment_date.format(i18n.translate('MOMENT_HISTORY_FORMAT'));
                list.push(<div className={style.list_separator} key={index}><h1>{separator_text}</h1><div>{block_date_list}</div></div>);  
                last_index = index;
            }

        }

        return (
            <ScrollView>
                {list}
            </ScrollView>
        );
    }
}

CallHistory.contextTypes = {
    router: React.PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        user: state.user,
        call: state.call
    };
};

const CallHistoryContainer = connect(
    mapStateToProps,
    {
        getHistory,
        dial
    }
)(CallHistory);

export default CallHistoryContainer;
