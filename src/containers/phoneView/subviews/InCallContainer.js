import { connect } from 'react-redux';
import React, { Component } from 'react';
import classNames from 'classnames';
import { hangup, isCalling } from 'src/actions/call';
import ScrollView from 'components/ScrollView';
import CallStateCloud from 'components/InCall/CallStateCloud';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'components/Button';
import style from 'scss/in_call.scss';
import i18n from 'src/i18n';
import {STATUS} from 'src/constants';
import {APP_CALL_ANDROID, APP_RESUME} from 'src/actionTypes';


class InCall extends Component {
    constructor() {
        super();
    }



    componentDidUpdate() {
        let {call} = this.props;  
        window.router = this.context.router;

        if ((typeof call.event.lineState !== 'undefined' && call.event.lineState === STATUS.HUNGUP) || Object.keys(call.current_call).length < 1)  {
            this.context.router.push('/history');
        }

    }

    componentDidMount() {
        let {onViewHideHeader, call} = this.props;  
        onViewHideHeader();
    }

     componentWillReceiveProps(nextProps) {

         if (typeof cordova !== 'undefined' && cordova.platformId === 'ios' && nextProps.app.event === APP_RESUME) {
             this.onCloseDial();
         }

    }

    onCloseDial(e) {
        e.preventDefault();
        e.stopPropagation();

        this.props.hangup();

        return false;
    }

    render() {
        let {call, app} = this.props;
        let line_state = 'OUTGOING';
        let status_call = i18n.translate(line_state);

        if (typeof call.event.lineState !== 'undefined') {
            line_state = call.event.lineState
            if (call.event.lineState === STATUS.RINGING) {
                status_call = i18n.translate('RINGING');    
            } else if (call.event.lineState === STATUS.UP) {
                status_call = i18n.translate('INCALL');
            } 
        }

        let cancel_button = [];

        if (Object.keys(call.current_call).length > 0 && app.event === APP_CALL_ANDROID) {
            if (app.data === 'RINGING') {
                status_call = i18n.translate('RINGING');  
            } else if (app.data === 'OFFHOOK') {
                status_call = i18n.translate('INCALL');
            } else {
               status_call = '';
            }
        }


        let transit_view = (<div className={style.call_info} dangerouslySetInnerHTML={{__html: i18n.translate('INTRANSIT')}} />);
        if (typeof cordova !== 'undefined' && cordova.platformId === 'ios') {
            transit_view = (
                <div className={style.call_info}>
                    <p style={{marginBottom: 30}} dangerouslySetInnerHTML={{__html: i18n.translate('INTRANSIT')}} />
                    <Button style={{width: '100%'}} type="cancel" onClick={this.onCloseDial.bind(this)}>{i18n.translate('CANCEL')}</Button>
                </div>
            );
        }

        return (
            <ScrollView>
                <div className={style.container}>
                    <div className={style.call_type_container}>
                        <CallStateCloud lineState={line_state}/>
                        <span className={style.call_type}>{i18n.translate('OUTGOING')}</span>
                    </div>
                    <div className={style.call_people}>

                        <h2>{call.current_call.name}</h2>
                        <span>{call.current_call.number}</span>
                        
                    </div>

                    {call.calling ? (

                        <div className={style.call_info}>
                            <p style={{marginBottom: 30}} dangerouslySetInnerHTML={{__html: status_call}} />

                            <a href={''} className={style.btn_cancel} onClick={this.onCloseDial.bind(this)}>
                                <SVGIcon filename={'hangup_big'} width={30}/>
                            </a>
                        </div>

                    ) : (
                       transit_view
                    )}
                    
                </div>
            </ScrollView>
        );
    }
}

InCall.contextTypes = {
    router: React.PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        app: state.app,
        call: state.call
    };
};

const InCallContainer = connect(
    mapStateToProps,
    {
        hangup,
        isCalling
    }
)(InCall);

export default InCallContainer;
