import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import i18n from 'src/i18n';
import { getFavorites, removeFavorite, initFavoritesEvents } from 'src/actions/favorites';
import { dial } from 'src/actions/call';
import ScrollView from 'components/ScrollView';
import Modal from 'components/Modal';
import Button from 'components/Button';
import FavoriteItem from 'components/Favorites/Item';
import PhoneNumberSelection from 'components/PhoneNumberSelection';

import style from 'scss/favorites.scss';

class Favorites extends Component {
    constructor() {
        super();
        this.state = {
            to_call: {}
        };
    }

    componentWillMount() {
        this.props.initFavoritesEvents();
        this.props.getFavorites();
    }

    onFavoriteButton(contact_id, source) {
        this.props.removeFavorite(contact_id, source);
    }

    onItemClick(favorite) {
        //this.props.displayModal(true, this.renderModal(to_call));

        let {displayModal} = this.props;
        let username = favorite.entry[0];
        let phone_numbers = [];
        for (let i = 1, l = 4; i < l; i++) {
            let entry = favorite.entry[i];

            if (entry.length > 0) {
                phone_numbers.push(favorite.entry[i]);
            }
        }

        if (phone_numbers.length > 1) { //Display Modal phone number selection
            displayModal(true, <PhoneNumberSelection onDial={this.onDialBtn.bind(this)} onClose={() => {displayModal(false, null)}} numbers={phone_numbers} username={username} />);
        } else {
            if (phone_numbers.length > 0) {
                this.onDialBtn(phone_numbers[0], username);
            } else {
                displayModal(true, (
                    <div>
                        <p>
                            Ce contact n'a pas de numéro de téléphone
                        </p>
                        <Button type="cancel" onClick={() => {displayModal(false, null);}}>{i18n.translate('OK')}</Button>
                    </div>
                ));
            }
        }
    }

    onDialBtn(destination, name) { 
        let {displayModal, dial, user} = this.props;
        let from_mobile = (user.mobileNumber && user.mobileNumber.length > 0);
        displayModal(false, null);
        dial(destination, name, from_mobile);
        this.context.router.push('/incall');
    }

    onCloseDial(e) {
        this.props.displayModal(false, null);
    }

    renderModal(to_call) {
        return (
            <div>
                {(typeof to_call.entry !== 'undefined' && to_call.entry.length > 0) ? (
                    <div>
                        <p>{i18n.translate('NUMBER_CALL_TXT')}</p>
                        <p>
                            {to_call.entry[0]}
                        </p>
                        <Button onClick={this.onDialBtn.bind(this, to_call.entry[1], to_call.entry[0])}>{i18n.translate('CALL')}</Button>
                    </div>
                    
                ) : (
                    <p>{i18n.translate('UNKNOWN_CALL_IMPOSSIBLE')}</p>
                )}

                <Button type="cancel" onClick={this.onCloseDial.bind(this)}>{i18n.translate('CANCEL')}</Button>
            </div>
        );
    }

    renderLoading() {

        return(
            <ScrollView>
                <div className={'loading'}>
                    <h1>{i18n.translate('FAVORITES')}</h1>
                    <p>{i18n.translate('LOADING')}</p>
                </div>
            </ScrollView>
        );
    }


    render() {
        let {favorites} = this.props;
        let _this = this;
        let list = (<div className={style.nothing}>{i18n.translate('FAVORITES_EMPTY')}</div>);

        
        if (!favorites.loaded) {
            return this.renderLoading();
        }
        

    
        if (favorites.data.length > 0) {
            list = favorites.data.map(function(favorite_item, index) {
                return <FavoriteItem key={index} item={favorite_item} onFavoriteButton={_this.onFavoriteButton.bind(_this)} onItem={_this.onItemClick.bind(_this, favorite_item)}/>;
            });
        }



        return (
            <ScrollView>
                {list}
            </ScrollView>
        );
    }
}


Favorites.contextTypes = {
    router: React.PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        favorites: state.favorites,
        user: state.user
    };
};

const FavoritesContainer = connect(
    mapStateToProps,
    {
        initFavoritesEvents,
        getFavorites,
        removeFavorite,
        dial
    }
)(Favorites);

export default FavoritesContainer;
