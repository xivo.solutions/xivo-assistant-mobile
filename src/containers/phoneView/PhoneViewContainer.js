import React, {Component} from 'react';
import { connect } from 'react-redux';

import HeaderContainer from 'containers/phoneView/partials/HeaderContainer';
import MenuContainer from 'containers/phoneView/partials/MenuContainer';
import PhoneNavigationContainer from 'containers/phoneView/partials/PhoneNavigationContainer';
import style from 'scss/phoneviewcontainer.scss';

import Modal from 'components/Modal';

import { initEvents } from 'src/actions/call';
import { triggerBackButton } from 'src/actions/app';
import { resetContext } from 'src/actions/user';
import {APP_CALL_ANDROID, APP_RESUME} from 'src/actionTypes';
import {STATUS} from 'src/constants';
import $ from 'jquery';

class PhoneView extends Component {

    constructor() {
        super();
        var _this = this;
        this.state = {
            open_menu: false,
            hide_header: false,
            modal: {
                hide: true,
                content: false,
                noStyle: false
            }
        };
        
        function onBackButton() {

            _this.props.triggerBackButton();

            if (_this.state.open_menu) {
                _this.setState({
                    open_menu: false
                });
                return false;
            }

            console.log('history', window.history_items.length, window.history_items);

            if ( window.history_items.length < 2) {

            } else {
               _this.context.router.goBack();
                window.history_items.pop();
            }
           
           
        }


        $(document).off('backbutton').on('backbutton', onBackButton);
    }


    componentWillReceiveProps(nextProps) {

         if (typeof cordova !== 'undefined' && nextProps.app.event === APP_RESUME) {
             console.log('socket', Cti.webSocket);
         }

         if (typeof cordova !== 'undefined' && nextProps.app.event === APP_RESUME && Cti.WebSocket.getSocket().readyState > 1) {
             let _this = this;
             let reactTimeout = setTimeout(function() {
                _this.context.router.push('/login');
                Cti.WebSocket.close();
                _this.props.resetContext();
                clearTimeout(reactTimeout);
             }, 250);
             
             console.log('APP:RESET-RESUME', Cti.webSocket);
             return false;
         }
         
         if (nextProps.app.event === 'APP_WEBSOCKET_OFFLINE') {
             //this.context.router.push('/login');
             // -> Maybe need a timeout fro correctly change the state
             let _this = this;
              let reactTimeout = setTimeout(function() {
                _this.context.router.push('/login');
                Cti.WebSocket.close();
                _this.props.resetContext();
                clearTimeout(reactTimeout);
             }, 250);

             console.log('APP_WEBSOCKET_OFFLINE', Cti.webSocket)
         }

    } 

    componentWillMount() {
        let {user} = this.props;
        this.checkUser();
        if (user.logged) {
            this.props.initEvents();
        }
    }

    componentDidMount() {
        let _this = this;
    }

    checkUser() {
        let {user} = this.props;
        if (!user.logged) {
            this.context.router.push('/login');
        }

    }

    componentDidUpdate() {
         this.checkUser();
         
        if (!this.context.router.isActive('/incall') && this.state.hide_header) {
            this.setState({
                hide_header: false
            });
        }
    }

    renderLoading() {
        return (
            <div>LOADING</div>
        );
    }



    onMenuBtn(ev) {
        if (typeof cordova !== 'undefined') {
            cordova.plugins.Keyboard.close();
        }

        if (typeof ev !== 'undefined' && typeof ev.preventDefault === 'function' && typeof ev.stopPropagation === 'function') {
            ev.preventDefault();
            ev.stopPropagation();
        }

        this.setState({
            open_menu: !this.state.open_menu
        });
        return false;
    }

    onViewHideHeader(state) {
        this.setState({
            hide_header: true
        });
    }

    displayModal(display, jsx, noStyle) {
        this.setState({
            modal: {
                hide: !display,
                content: jsx,
                noStyle
            }
        });
    }



    render() {
        let _this = this;
        let {user, children} = this.props;
        if (!user.logged && !user.loaded) {
            //return this.renderLoading();
        }

        var childrenWithProps = React.Children.map(children, function(child) {
            return React.cloneElement(child, {onViewHideHeader: _this.onViewHideHeader.bind(_this), displayModal: _this.displayModal.bind(_this)});
        });

        return (
            <div className={style.flex_container}>
                <Modal hide={this.state.modal.hide} noStyle={this.state.modal.noStyle}>
                    {this.state.modal.content}
                </Modal>
                <MenuContainer open={this.state.open_menu} onMenuBtn={this.onMenuBtn.bind(this)} displayModal={this.displayModal.bind(this)} />
                <HeaderContainer displayModal={this.displayModal.bind(this)} onMenuBtn={this.onMenuBtn.bind(this)} hide={this.state.hide_header}/>
                {childrenWithProps}
                <PhoneNavigationContainer displayModal={_this.displayModal.bind(_this)}/>
            </div>
        );
    }

}

PhoneView.contextTypes = {
    router: React.PropTypes.object
};



const mapStateToProps = (state) => {
    return {
        user: state.user,
        call: state.call,
        app: state.app
    };
};

const PhoneViewContainer = connect(mapStateToProps,{initEvents, triggerBackButton, resetContext})(PhoneView);

export default PhoneViewContainer;
