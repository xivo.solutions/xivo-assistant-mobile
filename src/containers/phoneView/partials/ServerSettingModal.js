import { connect } from 'react-redux';

import React, { Component } from 'react';
import style from 'scss/modal.scss';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'components/Button';
import {saveConstant, getConstant} from 'src/constants';
import classNames from 'classnames';
import i18n from 'src/i18n';

class ServerSettingModal extends Component {

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        return false;
    }

    validateFwdNumber() {
        let {onCloseBtn} = this.props;
        let {protocol, server_uri} = this.refs;
        saveConstant('WS_CONFIG', {URL: server_uri.value, PROTOCOL: protocol.value});
        onCloseBtn();
    }

    render() {
        let ws_config = getConstant('WS_CONFIG');
        let {user, onCloseBtn} = this.props;
        return (
            <form onSubmit={this.onSubmit.bind(this)} className={style.form_container}>
                <h4>{i18n.translate('MENU_XIVO_ADDRESS')}</h4>
    
                <div>
                    <label htmlFor={'protocol'}>
                        <span>
                            {i18n.translate('SERVER_KIND')}
                        </span>
                        <select id={'protocol'} ref={'protocol'} name={'protocol'} defaultValue={ws_config.PROTOCOL}>
                            <option value={'ws'}>WS://</option>
                            <option value={'wss'}>WSS://</option>
                        </select>
                    </label>
                </div>
                <div className={style.options_server}>
                    <label htmlFor={'server'}>
                           {i18n.translate('SERVER_URL')}
                    </label>
                    <input type="text" ref={'server_uri'} id="server" defaultValue={ws_config.URL} placeholder={i18n.translate('SERVER')}/>
                </div>

                <div className={style.horizontal_btns}>
                    <Button size={'small'} onClick={this.validateFwdNumber.bind(this)}>{i18n.translate('VALIDER')}</Button>
                    <Button size={'small'} type={'cancel'} onClick={onCloseBtn.bind(this)}>{i18n.translate('CANCEL')}</Button>
                </div>
                
            </form>
        );
    }
}



export default ServerSettingModal;
