import { connect } from 'react-redux';
import { login, logout, hangup, getFavorites, addFavorite } from 'src/actions/user';
import { dial, enableForward } from 'src/actions/call';
import React, { Component } from 'react';
import style from 'scss/header.scss';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'components/Button';
import PhoneStateModal from 'containers/phoneView/partials/PhoneStateModal';
import {getConstant, PHONE_SELECTOR} from 'src/constants';
import i18n from 'src/i18n';
import classNames from 'classnames';

class Header extends Component {

    constructor() {
        super();
        this.calling = false;
    }

    onLogout(e) {
        e.preventDefault();
        e.stopPropagation();


        return false;
    }

 


    onMessaging(e) {
        e.preventDefault();

        //this.props.displayModal(true, this.renderVoiceMailModal());

        return false;
    }

    onDialVoiceMailBtn() {
        let {dial, user} = this.props;
        let from_mobile = (user.mobileNumber && user.mobileNumber.length > 0)
        this.props.dial(getConstant('VOICEMAIL_NUMBER'), i18n.translate('VOICEMAIL'), from_mobile);
        this.context.router.push('/incall');
        this.onCancelVoiceMailCallBtn();
    }

    onCancelVoiceMailCallBtn() {
         this.props.displayModal(false, null);
    }

    onPhoneSelector() {
        let {user, displayModal, enableForward}= this.props;
        displayModal(true, <PhoneStateModal user={user} enableForward={enableForward} onClose={displayModal.bind(this, false, null)} />);
    }

    onClickPhoneSelectorBtn(type) {
        let {enableForward} = this.props;
        enableForward(type === PHONE_SELECTOR.FOWARD);
        this.props.displayModal(true, null);
    }



    renderVoiceMailModal() {
        let new_messages_nb = this.props.call.voicemail.newMessages;
        let has_messages = new_messages_nb > 0;


        return (
            <div>

                <p>{i18n.translate('VOICEMAIL')}</p>
                <p>
                     {new_messages_nb > 1 ? i18n.translate('VOICEMAIL_MULTIPLE', {number: new_messages_nb}) : i18n.translate('VOICEMAIL_SIMPLE', {number: new_messages_nb})}
                </p>
                {new_messages_nb > 0 ? (
                    <Button onClick={this.onDialVoiceMailBtn.bind(this)}>{i18n.translate('CALL')}</Button>
                ) : null}
                
                <Button type="cancel" onClick={this.onCancelVoiceMailCallBtn.bind(this)}>{i18n.translate('CANCEL')}</Button>
            </div>
        );
    }

    renderInCallHeader() {
        let {user, call, onMenuBtn, hide} = this.props;
        let header_classes = classNames(style.header, style.header_call_progress,{
            [`${style.header_hide}`]: hide
        })
        return (
            <div className={header_classes}>
                <span>APPEL EN COURS <br /> {call.current_call.number}</span>
                <div className={style.action_btn_group}>
                    
                    <div onClick={() => {}} className={style.call_action_btn} style={{backgroundColor: '#cd4747'}}>
                        <SVGIcon filename={'hangup_big'} width={30}/>
                    </div>
                    {/*<div className={style.call_action_btn} style={{backgroundColor: '#e67d39'}}>
                       <i className={'fa fa-phone'} /> 
                    </div>*/}
                    
                </div>
            </div>
        );
    }

    render() {
        let {user, call, onMenuBtn, hide} = this.props;
        let header_classes = classNames(style.header, {
            [`${style.header_hide}`]: hide
        })

        return (
            <div className={header_classes}>
                <SVGIcon filename={'logo_xivo_small'} width={55} style={{position: 'absolute', top: 13, left: 13}}/>
                <div className={style.action_btn_group}>
                    <a href={''} onClick={onMenuBtn.bind(this)} className={style.menu_icon}>
                        <i className={'fa fa-bars'}/>
                    </a>

                    <span className={style.phone_selector} onClick={this.onPhoneSelector.bind(this)}>
                        <SVGIcon filename={user.uncFwdEnabled ? 'renvoi_state' : 'mobile_state'} width={58}/>
                        <SVGIcon className={style.arrow_selector} filename={'chevron_down'} width={17}/>
                    </span>

                    <a href={''} onClick={this.onMessaging.bind(this)}>
                        <SVGIcon filename={'voicemail'} width={30}/>

                        {(call.voicemail.newMessages > 0) ? (

                            <span className={style.messaging_notification}>
                            {call.voicemail.newMessages}
                            </span>

                        ) : null}
                        
                    </a>
                </div>
            </div>
        );
    }
}

Header.contextTypes = {
    router: React.PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        call : state.call
    };
};

const HeaderContainer = connect(
    mapStateToProps,
    {
        logout,
        dial,
        enableForward
    }
)(Header);

export default HeaderContainer;
