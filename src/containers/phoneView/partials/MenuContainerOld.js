import { connect } from 'react-redux';
import { login, logout} from 'src/actions/user';
import React, { Component } from 'react';
import style from 'scss/menu.scss';
import SVGIcon from 'components/tools/SVGIcon';
import classNames from 'classnames';

class Header extends Component {

    onLogoutBtn(e) {
        e.preventDefault();
        e.stopPropagation();

        this.props.logout();

        return false;
    }

    render() {
        let {user, call, open, onMenuBtn} = this.props;

        let classes_container = classNames(style.container, {
            [`${style.open}`]: open
        });

        return (
            <div className={classes_container} onClick={onMenuBtn.bind(this)}>
                <div className={style.flex}>
                    <header>
                        <h2>{user.fullName}</h2>
                        <span>[10490]</span>
                    </header>

                    <div className={style.listing}>
                        <div>
                            <h5>PREFERENCES</h5>
                            <ul>
                                <li>Use too my phone's contacts</li>
                                <li>Numéro de renvoi : 5965</li>
                            </ul>
                        </div>

                        <div>
                            <h5>PARAMÈTRES</h5>
                            <ul>
                                <li>Adresse du serveur XiVO</li>
                                <li>Point d’accès WIFI autorisé</li>
                                <li>N° de mobile utilisé pour le call back</li>
                            </ul>
                        </div>
                    </div>

                    <a href={''} onClick={this.onLogoutBtn.bind(this)} className={style.logout_container}>
                        <SVGIcon filename={'connexion'} width={50}/>
                        <span>DÉCONNEXION</span>
                    </a>
                </div>
            </div>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

const HeaderContainer = connect(
    mapStateToProps,
    {
        logout
    }
)(Header);

export default HeaderContainer;
