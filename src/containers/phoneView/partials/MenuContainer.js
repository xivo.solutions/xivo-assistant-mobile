import { connect } from 'react-redux';
import { login, logout, useLocalContacts} from 'src/actions/user';
import { setForwardNumber} from 'src/actions/call';
import React, { Component } from 'react';
import style from 'scss/menu.scss';
import SVGIcon from 'components/tools/SVGIcon';
import FwdModal from 'containers/phoneView/partials/FwdModal';
import ServerSettingModal from 'containers/phoneView/partials/ServerSettingModal';
import * as Consts from 'src/constants';
import classNames from 'classnames';
import i18n from 'src/i18n';
import Hammer from 'react-hammerjs';



import OptionsComponent from 'containers/loginView/partials/OptionsComponent';

class Header extends Component {




    onLogoutBtn(e) {
        e.preventDefault();
        e.stopPropagation();
        this.props.logout();
        this.context.router.push('/login');
       
        return false;
    }

    onUseLocalContact(e) {
        e.preventDefault();
        e.stopPropagation();

        let {user, useLocalContacts} = this.props;
        let use_local = !user.use_local_contacts;
        useLocalContacts(use_local);
       

        return false;
    }

    onFwdNumber() {
        let {displayModal, user, setForwardNumber} = this.props;
        displayModal(true, <FwdModal user={user} setForwardNumber={setForwardNumber} onCloseBtn={displayModal.bind(this, false, null)} />);
    }

    onXIVOSetting() {
        let {displayModal} = this.props;
        displayModal(true, <OptionsComponent hasCancelBtn={true} onSubmitOptionsAction={displayModal.bind(this, false, null)} />, true);
    }


    onSwipe(ev) {
        let {onMenuBtn} = this.props;
        let delta_min = 100;
        if (ev.direction === Consts.SWIPE_LEFT && ev.deltaX < -delta_min) {
            onMenuBtn.call(this);
        }
    }

    insideMenu(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    render() {
        let {user, call, open, onMenuBtn} = this.props;

        let classes_container = classNames(style.main_container, {
            [`${style.open}`]: open,
            [`${style.close}`]: !open
        });


        return (
            <Hammer onSwipe={this.onSwipe.bind(this)}>
                <div className={classes_container} onClick={onMenuBtn.bind(this)}>
                    <div className={style.container} onClick={this.insideMenu.bind(this)}>
                        <div className={style.flex}>
                            <header>
                                <h2>{user.fullName}</h2>
                                {/*<span>[]</span>*/}
                            </header>

                            <div className={style.listing}>
                                <div>
                                    <h5>{i18n.translate('MENU_SECTION_PREFS')}</h5>
                                    <ul>
                                        <li onClick={this.onUseLocalContact.bind(this)}>
                                            <span>{i18n.translate('MENU_USE_PHONE_CONTACTS')}</span> 
                                            {user.use_local_contacts ?  <SVGIcon filename='check' width={17}/> : null}
                                        </li>

                                        <li onClick={this.onFwdNumber.bind(this)}>
                                            <span>{i18n.translate('MENU_CALL_FORWARDING')} {user.uncFwdDestination}</span> 
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    <h5>{i18n.translate('MENU_SECTION_SETTINGS')}</h5>
                                    <ul>
                                        <li onClick={this.onXIVOSetting.bind(this)}>
                                            <span>{i18n.translate('MENU_XIVO_ADDRESS')}</span> 
                                        </li>

                                        {user.mobileNumber && user.mobileNumber.length > 0 ? (
                                            <li className={style.two_lines}>
                                                <span>{i18n.translate('MENU_CALLBACK_NUMBER')}</span> 
                                                <span className={style.bold}>{user.mobileNumber}</span>
                                            </li>
                                        ) : null}
                                        
                                    </ul>
                                </div>
                            </div>

                            <div className={style.menu_footer}>
                                <div className={style.version}> Version: {Consts.VERSION} </div>

                                <a href={''} onClick={this.onLogoutBtn.bind(this)} className={style.logout_container}>
                                    <span className={style.logout_btn}>
                                        <i className={'fa fa-power-off'} /> 
                                    </span>
                                    <span>{i18n.translate('MENU_LOGOUT')}</span>
                                </a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </Hammer>
        );
    }
}

Header.contextTypes = {
    router: React.PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

const HeaderContainer = connect(
    mapStateToProps,
    {
        logout,
        useLocalContacts,
        setForwardNumber
    }
)(Header);

export default HeaderContainer;
