
import React, { Component } from 'react';
import { connect } from 'react-redux';

import classNames from 'classnames';
import {APP_KEYBOARD_OPEN, APP_KEYBOARD_CLOSE, APP_CALL_ANDROID, APP_BACKBUTTON} from 'src/actionTypes';

import { search, resetDirectory } from 'src/actions/directory';
import { hangup, isCalling } from 'src/actions/call';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'components/Button';
import TouchableLink from 'components/TouchableLink';
import i18n from 'src/i18n';
import {STATUS} from 'src/constants';

import $ from 'jquery';


import style from 'scss/phone_navigation.scss';

class PhoneNavigation extends Component {
    constructor() {
        super();
        this.state = {
            onSearch: false,
            route: '',
            searchPositionBottom: 0
        };
        this.calling = false;
    }






    componentWillReceiveProps(nextProps) {

        if (nextProps.app.event === APP_KEYBOARD_OPEN) {
            this.setState({
                searchPositionBottom: nextProps.app.data
            });
        }
        
        if (nextProps.app.event === APP_BACKBUTTON) {
            if (this.state.onSearch) {
                this.setState({
                    'onSearch': false
                });
                return false;
            }
        }

        if (nextProps.app.event === APP_KEYBOARD_CLOSE) {
            this.setState({
                searchPositionBottom: 0
            });
        }

        if (Object.keys(nextProps.call.current_call).length > 0 && (nextProps.app.event === APP_CALL_ANDROID && nextProps.app.data === 'OFFHOOK')){
            this.props.isCalling(true);
        }


        if ((nextProps.app.event === APP_CALL_ANDROID && nextProps.app.data === 'IDLE')) {
            this.props.isCalling(false);
            nextProps.hangup();
        }

        
    }

    onSearchBtn(e) {
        e.preventDefault();

        this.setState({
            onSearch: !this.state.onSearch
        });

        var t = setTimeout(() => {
            if (this.state.onSearch) {
                $(this.refs.search).focus();
            } else {
                $(this.refs.search).blur();
            }
             
             clearTimeout(t);
        }, 10); 

        return false;
    }   

    onSubmitSearch(e) {
        e.preventDefault();
        e.stopPropagation();
        let {user, displayModal, resetDirectory} = this.props;

        var term = this.refs.search.value;

        if (term.length < 2) {
            displayModal(true, (
                <div>
                    <p style={{margin: 30, lineHeight:'18px'}}>{i18n.translate('SEARCH_MIN_LENGTH')}</p>
                    <Button onClick={this.props.displayModal.bind(this, false, null)}>{i18n.translate('OK')}</Button>
                </div>
            ));
            return false;
        }
        
        this.refs.search.value = '';
        if (typeof cordova !== 'undefined') {
            cordova.plugins.Keyboard.close();
        }
        
        resetDirectory();
        displayModal(false, null);
        if (!this.context.router.isActive('/directory')) {
            this.context.router.push('/directory');
        }
        this.props.searchInDirectory(term, user.use_local_contacts);

        return false;
    };

    onNavigation(route) {
        this.setState({
            route
        });
    }




    render() {
        let {call, app} = this.props;
        let disableClasse = classNames(style.round_btn,
            {
                [`${style.disable_btn}`]: true
            });


        let classes = classNames(style.search_container, {
            'anim': this.state.onSearch
        });

        let inputClasses = classNames({
            'anim': this.state.onSearch
        });


        let _this = this;
        return (
            <div className={style.height_fix} style={{}}>
                <div className={style.container}>
                        <div className={style.btn_container}>
                            <div className={style.btn_group}>
                                <TouchableLink onClick={function(){_this.onNavigation(this.to)}} to="/history"> <i className={'fa fa-history'} /> </TouchableLink>
                                <TouchableLink onClick={function(){_this.onNavigation(this.to)}} to="/favorites"> <i className={'fa fa-star'} /> </TouchableLink>
                                <TouchableLink style={{position: 'relative'}} onClick={function(e){
                                    e.preventDefault();
                                    e.stopPropagation();
                                    console.log('call', call.current_call, this.to, call)

                                    if (call.current_call && call.current_call.number) {
                                        _this.onNavigation(this.to);
                                        if (!_this.context.router.isActive(this.to)) {
                                            _this.context.router.push(this.to);
                                        }
                                    } else {
                                        return false;
                                    }

                                    
                                }} to="/incall"> 
                                    <i className={'fa fa-phone'} /> 
                                    {call.calling ? (
                                        <span className={style.notification_badge}>1</span>
                                    ) : null}
                                </TouchableLink>
                            </div>
                            <a className={style.round_btn} href={''} onClick={this.onSearchBtn.bind(this)}>
                                <i className={'fa fa-search'} />                        
                            </a>
                        </div>
                        <div className={classes}>
                            <div className={style.back_button} onClick={this.onSearchBtn.bind(this)}>
                                <SVGIcon className={style.back} filename={'search_back'} width={13}/>
                            </div>
                            <form onSubmit={this.onSubmitSearch.bind(this)}>
                                <input  autoComplete="off" autoCorrect="off" autoCapitalize="off" id={'search'} ref={'search'} type={'search'} className={inputClasses} />
                            </form>
                        </div>
                    
                </div>
            </div>
        );
    }
}

PhoneNavigation.contextTypes = {
    router: React.PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        call: state.call,
        app: state.app
    };
};

const PhoneNavigationContainer = connect(mapStateToProps,{
    searchInDirectory: search,
    resetDirectory,
    hangup,
    isCalling
})(PhoneNavigation);
export default PhoneNavigationContainer;
