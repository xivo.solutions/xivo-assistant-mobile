import { connect } from 'react-redux';
import React, { Component } from 'react';
import style from 'scss/phone_state_modal.scss';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'components/Button';
import classNames from 'classnames';
import i18n from 'src/i18n';
import {PHONE_SELECTOR} from 'src/constants';

class PhoneStateModal extends Component {

    constructor() {
        super();
        this.state = {
            is_fwd: false,
            can_be_enable: true
        };
    }

    componentWillMount() {
        this.setState({is_fwd: this.props.user.uncFwdEnabled});
    }

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }


    onClickPhoneSelectorBtn(type) {
        let {user, enableForward, onClose} = this.props;
        
        if (user.uncFwdDestination.length > 0 && user.uncFwdDestination !== 'null') {
            let is_fwd = (type === PHONE_SELECTOR.FOWARD);
            this.setState({can_be_enable: true, is_fwd});
            enableForward(is_fwd);
             onClose.call(this);
        } else {
            this.setState({can_be_enable: false});
        }
        
    }

    render() {
        let {onClose} = this.props;
        let is_fwd = this.state.is_fwd;

        return (
            <div className={style.phone_selector}>
                <h4>{i18n.translate('PHONE_USE_TYPE')}</h4>
                <p dangerouslySetInnerHTML={{__html: i18n.translate('PHONE_USE_TYPE_MODAL_TXT')}} />

                {!this.state.can_be_enable ? (
                    <p className={style.msg_error}>{i18n.translate('PHONE_USE_TYPE_MODAL_ERROR_TXT')}</p>
                ) : null}

                <div className={style.selector_container}>
                    <SVGIcon onClick={this.onClickPhoneSelectorBtn.bind(this, PHONE_SELECTOR.FOWARD)} filename={`state_liste_renvoi_${is_fwd ? 'selected' : 'default'}`} width={60}/>    
                    <SVGIcon onClick={this.onClickPhoneSelectorBtn.bind(this, PHONE_SELECTOR.MOBILE)} filename={`state_liste_mobile_${!is_fwd ? 'selected' : 'default'}`} width={60}/>    
                </div>
                <div className={style.btn_container}>
                    <Button size={'small'} type={'cancel'} onClick={onClose.bind(this)}>{i18n.translate('CANCEL')}</Button>
                </div>
            </div>
        );
    }
}

export default PhoneStateModal;
