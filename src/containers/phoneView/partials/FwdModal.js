import { connect } from 'react-redux';
import React, { Component } from 'react';
import style from 'scss/modal.scss';
import SVGIcon from 'components/tools/SVGIcon';
import Button from 'components/Button';
import classNames from 'classnames';
import i18n from 'src/i18n';

class FwdModal extends Component {

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        return false;
    }

    validateFwdNumber() {
        let {onCloseBtn, setForwardNumber} = this.props;
        setForwardNumber(this.refs.fwd_destination.value);
        onCloseBtn();
    }

    render() {
        let {user, onCloseBtn} = this.props;
        return (
            <form onSubmit={this.onSubmit.bind(this)} className={style.form_container}>
                <h4>{i18n.translate('PHONE_FORWARD')}</h4>
                <p> 
                    {i18n.translate('PHONE_FORWARD_MODAL_TXT')}
                </p>
                <input ref={'fwd_destination'} type={'text'} name={'fwd_destination'} defaultValue={user.uncFwdDestination} />
                <div className={style.horizontal_btns}>
                    <Button size={'small'} onClick={this.validateFwdNumber.bind(this)}>{i18n.translate('VALIDATE')}</Button>
                    <Button size={'small'} type={'cancel'} onClick={onCloseBtn.bind(this)}>{i18n.translate('CANCEL')}</Button>
                </div>
                
            </form>
        );
    }
}

export default FwdModal;
