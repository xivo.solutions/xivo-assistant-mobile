import React, {Component} from 'react';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';

import PhoneViewContainer from 'containers/phoneView/PhoneViewContainer';
import AuthContainer from 'containers/loginView/AuthContainer';
import ViewportContainer from 'containers/ViewportContainer';
import CallHistoryContainer from 'containers/phoneView/subviews/CallHistoryContainer';
import FavoritesContainer from 'containers/phoneView/subviews/FavoritesContainer';
import DirectoryContainer from 'containers/phoneView/subviews/DirectoryContainer';
import InCallContainer from 'containers/phoneView/subviews/InCallContainer';

window.history_items = [];
const handleEnterRoute = (ev) => {
    let pathname = ev.location.pathname;
    if (window.history_items.length < 1 || window.history_items[window.history_items.length - 1] !== pathname) {
        window.history_items.push(pathname);
    }
   
};


export default (
    <Route path='/' component={ViewportContainer}>
    	<IndexRoute component={AuthContainer}/>
    	<Route path='/login' component={AuthContainer}/>
    	<Route path='/index' component={PhoneViewContainer} >
            <Route path='/history' component={CallHistoryContainer} onEnter={handleEnterRoute.bind(this)} />
            <Route path='/favorites' component={FavoritesContainer} onEnter={handleEnterRoute.bind(this)} />
            <Route path='/directory' component={DirectoryContainer} onEnter={handleEnterRoute.bind(this)} />
            <Route path='/incall' component={InCallContainer} onEnter={handleEnterRoute.bind(this)} />
        </Route>
    </Route>
);
