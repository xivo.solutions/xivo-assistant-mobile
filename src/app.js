import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { Router, browserHistory, hashHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux';
import routes from 'src/routes';
import * as constants from 'src/constants';
import configureStore from 'src/configureStore';
import i18n from 'src/i18n';
import moment from 'moment';
import LocalDatas from 'src/LocalDatas';
import jQuery from 'jquery';

import style from 'scss/app.scss';

document.addEventListener((typeof cordova !== 'undefined' ? 'deviceready' : 'DOMContentLoaded'), initApp, false);



function initApp(event) {
    console.log('XiVo - Assistant - startup');


           

    const initialState = window.__INITIAL_STATE__;

    const store = configureStore(initialState, hashHistory);

    i18n.loadLocalization(constants.getConstant('DEFAULT_LANG'));
    moment.locale(i18n.lang);


    if (typeof cordova !== 'undefined') {
        cordova.plugins.Keyboard.disableScroll(true);
    }

    //Debugging Redux store
    window.$ = jQuery;
    window.store = store;
    window.LocalDatas = LocalDatas;

    if (LocalDatas.get('USER_USE_LOCAL_CONTACTS') === null) {
        LocalDatas.set('USER_USE_LOCAL_CONTACTS', 0);
    }

    const app_el = document.getElementById('app')
    if (__DEV__) {
        const RedBox = require('redbox-react').default;
        try {
            render(
                <Provider store={store}>
                    <Router history={hashHistory} routes={routes} />
                </Provider>,
                app_el
            );
        } catch (e) {
            render(<RedBox error={e} />, app_el);
        }
    }

    render(
        <Provider store={store}>
            <Router history={hashHistory} routes={routes} />
        </Provider>,
        app_el
    );
};

if (module.hot) {
    module.hot.accept();
}


