# XiVo Assistant
 

## Quick start
Run npm command `npm install` first to initialize your project.  

## Development mode
Command `npm start` will start a local webserver on `http://localhost:3000` and serve the application without cordova.  

## Production mode
**BECARE FULL : you need to increase the version number in `package.json` in `version` parameter it will be sync with the Android/iOS app version**  

Command `npm run deploy` will deploy an application in production or in beta testing and it will build an app and prepare it to be embed with cordova.  

